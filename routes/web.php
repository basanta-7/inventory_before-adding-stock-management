<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// frontend
// -------------------------------------------- front end -------------------------------------------------
Route::get('item/category/{slug}', ['as' => 'item.getproducts', 'uses' => 'Frontend\ProductController@showproducts']);
// Route::get('/item/fetch','Frontend\ProductController@fetch'); //fetchforsorting
// Route::post('item/index1',['as' => 'item.index1', 'uses' => 'Frontend\ProductController@index']);
Route::get('/','Frontend\ProductController@index');
Route::get('/about','Frontend\PageController@getAbout');

Route::get('/contact-us','Frontend\PageController@getContact');
Route::post('/contact-us','Frontend\PageController@postContact');

// store all product listing 
Route::get('/store',['as' => 'item.sort', 'uses' => 'Frontend\ProductController@getstore']);

// Route::get('/getCategoryProduct/{id}',['as'=>'item.getCategoryProduct','uses' =>'Frontend\ProductController@getCategoryProduct']);
// Route::get('/getSubCategoryProduct/{id}',['as'=>'item.getSubCategoryProduct','uses' =>'Frontend\ProductController@getSubCategoryProduct']);
Route::resource('/item','Frontend\ProductController');

//search for products in frontend
Route::get('/searchitem',['as' =>'item.getsearch', 'uses' => 'Frontend\ProductController@getSearch']);

//for product enquiry form
Route::resource('enquire','EnquiryController');
// enquiry mail
// Route::get('/enquire/{slug}','Frontend\ProductController@getEnquire');
// Route::post('/enquire','Frontend\ProductController@postEnquire');

// frone end ends------------------------------------------------------------------------

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::group(['middleware' => ['auth']], function(){
	// products
	Route::post('product/fetch','ProductController@fetch');
	Route::post('product/fetchsub','ProductController@fetchsub');
	Route::post('product/fetchsubcat1','ProductController@fetchsubcat1');
	Route::delete('product/delete/{slug}', ['as' => 'product.destroysearch', 'uses' => 'ProductController@destroysearch']);
	Route::resource('product','ProductController');

	Route::get('/search','ProductController@getSearch');
	

	// Route::get('/getcategoryproduct/{category_id}','ProductController@getCategoryProduct');
	// Route::get('/getsubcategoryproduct/{subcategory_id}','ProductController@getSubCategoryProduct');
	// Route::get('/getsubcategory1product/{subcategory_id}','ProductController@getSubCategory1Product');

	// categories
	Route::get('category/{category_id}/subcategory','CategoryController@editSubCategory');
	Route::resource('category','CategoryController',['except' => ['create'],['destroy']]);

	// update subcategory
	Route::post('subcategory/', ['as' => 'subcategory.update', 'uses' => 'CategoryController@updatesubcategory']);
	// dashboard
	Route::get('/dashboard', 'PageController@getDashboard');
	// getting about page
	Route::get('/getabout', ['as' => 'page.getabout', 'uses' => 'PageController@getAbout']);
	Route::post('postabout', ['as' => 'page.about', 'uses' => 'PageController@postAbout']);
	// getting contact page
	// Route::get('/getcontact', 'PageController@getContact');
	// Route::post('postcontact', ['as' => 'page.contact', 'uses' => 'PageController@postcontact']);

	//user registration
	Route::group(['middleware' => ['user']],function(){
		Route::resource('users','UserController');
	});
	Route::get('/profile/{user_id}','UserController@getProfile');
	Route::post('/profile/edit',['as' => 'profile.edit', 'uses' => 'UserController@updateInformation']);
	Route::get('enquiries/{slug}',['as' => 'enquire.list','uses' => 'EnquiryController@showenquiries']);
});
