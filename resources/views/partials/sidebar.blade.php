 <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('img/favicon1.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name}}</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="{{url('search')}}" method="GET" class="sidebar-form">
        {{ csrf_field()}}
        <div class="input-group">
          <input type="text" name="searchtext" class="form-control" placeholder="Search for product">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span> 
        </div>
         <!-- <div class="radio" style="text-align: center; color: white;">
            <label class="radiolabel"> Product</label> <input type="radio" name="optradio" class="radiobutton" value="products" checked>
            <label class="radiolabel"> Category</label><input type="radio" name="optradio" class="radiobutton" value="categories">
        </div>  -->  
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">HEADER</li>
        <li class="treeview">
          <a href="#"><i class="fas fa-box-open"></i> <span>Product</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('product.create')}}"><i class="fa fa-plus-circle"></i>Add Product</a></li>
            <li><a href="{{ route('product.index')}}"><i class="fas fa-tasks"></i> Manage Product</a></li>
            <li><a href="{{ route('enquire.index') }}"><i class="far fa-question-circle"></i>  Enquiry Messages</a></li>
          </ul>
        </li>

        <!-- <li class="treeview">
          <a href="#"><i class="fas fa-tasks"></i> <span> Stock Management</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=""><i class="fas fa-shopping-cart"></i> Purchase</a></li>
            <li><a href=""><i class="fas fa-tasks"></i> Sales</a></li>
            <li><a href=""><i class="far fa-question-circle"></i>  View Stock</a></li>
          </ul>
        </li> -->
        
        <li>
          <a href="{{ route('category.index')}}">
            <i class="fa fa-bullseye"></i>
            <span>Categories</span>
          </a>
        </li>
        
        @if(Auth::user()->role == 'admin')
        <li class="treeview">
          <a href="#"><i class="fas fa-users-cog"></i> <span>User Management</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('users.create') }} "><i class="fa fa-plus-circle"></i> Add User </a></li>
            <li><a href="{{ route('users.index')}}"><i class="fas fa-tasks"></i> Manage User</a></li>
          </ul>
        </li>
        @endif

        <li class="treeview">
          <a href="#"><i class="fa fa-book"></i> <span>Page Management</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('getabout')}}">About Us</a></li>
            <!-- <li><a href="/getcontact">Contact Us</a></li> -->
          </ul>
        </li>
        <!-- Optionally, you can add icons to the links -->
        <!-- <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
        <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li> -->
      </ul>
      <!-- /.sidebar-menu -->
    </section>

    <style>
      .radiolabel{
        margin-right: 30px; 
        margin-top: -15px;
      }
      .radiobutton{
        margin-top:6px; 
        cursor: pointer;
      }
      
    </style>

    
    <!-- /.sidebar -->
  </aside>
