@extends('layouts.app')
@section('title','| About Page') 
@section('pageheader','Manage') 
@section('pageminiheader','About Page')
@section('navigate')
  <ol class="breadcrumb">
    <li><i class="fa fa-book"></i> Page Management</a></li>
    <li class="active"> About </li>
  </ol>
@endsection 
@section('content')
    <!-- Main content -->
    <section class="content">
      {!! Form::open(array('route' => 'page.about','files' => 'true')) !!}
      {{ csrf_field() }}
      <div class="col-md-8">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Content</h3>
          </div>
        
          <div class="box-body">
            <div class="form-group">
              <textarea name="content" id="" class="form-control" value="" rows="50">{{$contents->content}}</textarea>
              <!-- {{ Form::textarea('content', $value = null, ['class' => 'form-control', 'rows' => 3]) }} -->
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button type="reset" class="btn btn-danger">Reset</button>  
            </div>
          
          </div>
        </div>
      </div>
      
      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Image</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <img src="{{ asset('images/pages/'.$contents->image) }}" alt="" class="img-fluid" style="object-fit: contain;height: 390px;width: auto;">
            </div>  
            <div class="form-group">
              <!-- {{ Form::file('image', ['class'=>'form-control']) }} -->
              <input type="file" name="image" class="form-control" value="{{$contents->image}}">
            </div>
          </div>

          </div>
        </div>

      {!! Form::close(); !!}
    </section>
    <!-- /.content -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection
