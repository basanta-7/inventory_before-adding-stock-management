@extends('layouts.app')
@section('title','| Add User')
@section('pageheader','User')
@section('pageminiheader','')
@section('navigate')
  <ol class="breadcrumb">
    <li><a href="{{route('users.index')}}"><i class="fas fa-users-cog"></i> User</a></li>
    <li class="active">Add User</li>
  </ol>
@endsection
@section('content')
    {!! Form::open(array('route' => 'users.store','id'=>'submitform')) !!}
    {{csrf_field() }}
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Add User</h3>
        </div>

        <div class="box-body">
          <div class="form-group">
            {{ Form::label('name','Name') }}
            {{ Form::text('name',null,array('class' => 'form-control')) }}
          </div>   
          
          <div class="form-group">
            {{ Form::label('email','Email') }}
            {{ Form::email('email',null,array('class' => 'form-control')) }}
          </div>    
          
          <div class="form-group">
            {{ Form::label('password','Password') }}
            {{ Form::password('password',array('class' => 'form-control password')) }}
          </div>

          <div class="form-group">
            {{ Form::label('repassword','Re-password') }}
            {{ Form::password('repassword',array('class' => 'form-control repassword')) }}    
            <label id="invalidpassword" class="" style="color: red; display: none;">Password and re-password doesnt match</label>
          </div>

          <div class="form-group">
            {{ Form::label('role','Role') }}
            <select name="role" id="" class="form-control">
              <option value=""> ------Select One-------</option>
              <option value="user">User</option>
              <option value="admin">Admin</option>
            </select>
          </div>

          <div class="form-group" style="margin-top: 20px;">
            <button type="submit" class="btn btn-primary">Add User</button>
            <button type="reset" class="btn btn-danger">Reset</button>  
           </div>

        </div>
        </div>
      {!! Form::close() !!}
    </div>
  </div>
@endsection

@section('javascripts')
<script>
  $('#submitform').submit(function(e){
    var pass = $('.password').val();
    var repass = $('.repassword').val();
    
    if(pass != repass){
        $('#invalidpassword').show();
        e.preventDefault();
    }
    
  });
</script>
@endsection