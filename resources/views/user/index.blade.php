
@extends('layouts.app')
@section('title','| View Users') 
@section('pageheader','User') 
@section('pageminiheader','View All Users')
@section('navigate')
  <ol class="breadcrumb">
    <li><i class="fas fa-users-cog"></i> User</a></li>
    <li class="active">View All Users</li>
  </ol>
@endsection 
@section('content')
<div class="col-sm-12 col-md-12 col-lg-12">
    <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th class="th-sm">#
            </th>
            <th class="th-sm">Name
            </th>
            <th class="th-sm">Email
            </th>
            <th class="th-sm">Role
            </th>
            <th class="th-sm">Actions
            </th>
        </tr>
    </thead>
    <tbody>
        @php
            $count = 1;
        @endphp
        @foreach($users as $user)
            @if($user->id != 1)
                <tr>
                    <td>@php echo $count++ @endphp</td>
                    <td><a href="{{ url('profile/'. $user->id) }}">{{ $user->name }}</a></td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role }}</td>
                    <td>
                        <a href="{{ route('users.edit', $user->id)}}" class="btn pull-left" id="editbtn"><i class="fas fa-pen" title="edit"></i></a> 
                        <!-- delete button -->
                        {{ Form::open(['route' => ['users.destroy', $user->id], 'method' => 'DELETE', 'id' => 'deleteform', 'onsubmit' => 'deletecheck(event)'] ) }} 
                            <!-- {!!  Form::submit('Remove User',['class' =>'btn btn-danger btn-xs pull-left deletebtn','style' =>'margin-left : 5px', 'id' => 'deletebtn']) !!} -->
                            <button type="submit" id="deletebtn" class="deletebtn"><i class="fas fa-trash-alt" title="delete"></i></button>
                        {!! Form::close() !!}
                        <!-- delete button -->
                    </td>
                </tr>
            @endif
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>#</th>
            <th>Name
            </th>
            <th>Email
            </th>
            <th>Role
            </th>
            <th>Actions
            </th>
        </tr>
    </tfoot>
    </table>

    <style>
          .deletebtn{
            color: red;
            margin-top: 4px;
            background: none;
            border: none;
          }      
    </style>

</div>
@endsection
@section('javascripts')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#dtBasicExample').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });
    </script>

    <script>
        function deletecheck(e)
        {          
            if(confirm('Are you Sure you want to remove user ?'))
            {
                $(this).submit();
            }
            else
            {
                e.preventDefault();
            }
        }    
            // $('.deletebtn').click(function(e){
            //     if(confirm('Are you Sure you want to remove user ?')){
            //         $('#deleteform').submit();
            //     }
            //     else{
            //         e.preventDefault();
            //     }
            // e.preventDefault();
            // });
    </script>

<!--data table ends  -->
@endsection
