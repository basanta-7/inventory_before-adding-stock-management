@extends('layouts.app')
@section('title','| Edit User') 
@section('pageheader','User') 
@section('pageminiheader','Edit User')
@section('navigate')
  <ol class="breadcrumb">
    <li><a href="{{route('users.index')}}"><i class="fas fa-users-cog"></i> User</a></li>
    <li class="active">Edit User</li>
  </ol>
@endsection 
@section('content')
  <!-- add product form -->
  {!! Form::model($user, ['route' => ['users.update',$user->id],'method' =>'PUT']) !!}
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit User</h3>
        </div>
        
        <div class="box-body">
          <div class="form-group">
            {{ Form::label('name','Name') }}
            {{ Form::text('name',null,array('class' => 'form-control')) }}
          </div>
          
          <div class="form-group">
            {{ Form::label('email','Email') }}
            {{ Form::email('email',null,array('class' => 'form-control')) }}
          </div>  
                    
          <div class="form-group">
            {{ Form::label('role','Role') }}
            <select name="role" id="" class="form-control">
              @if($user->role == 'user')
                <option value="user">{{$user->role}}</option>
                <option value="admin">Admin</option>
              @else
                <option value="admin">{{$user->role}}</option>
                <option value="user">User</option>
            
              @endif
            </select>
          </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Update User</button>
                <a href="{{ route('users.index')}}" class="btn btn-danger">Cancel</a>  
            </div>
          </div>
        </div>
      </div>
  {!! Form::close() !!}

  <!-- form ends -->
@endsection
