@extends('layouts.app')
@section('title','| Search result')
@section('pageheader','Categories')
@section('pageminiheader','Search Results')
@section('content')
<style type="text/css">
	.subcategorys{list-style: none;}
	.subcategorys li{margin-bottom: 5px;}
</style>
<div class="row">
	<div class="col-sm-8 col-md-8 col-lg-8">
		<table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
		    <thead>
		        <tr>
		            <th class="th-sm">#
		            </th>
		            <th class="th-sm">Name
		            </th>
		            <th class="th-sm">Actions
		            </th>
		        </tr>
		    </thead>
		    <tbody>
		    	@php
            		$count = 1;
       			@endphp
		        @foreach($categories as $category)
			       	@if($category->pid == 0)
				        <tr>
				            <td>@php echo $count++ @endphp</td>
				            <td>
					           	<h4><a href="/getcategoryproduct/{{$category->id}}">{{ $category->name}}</a> 
					           		<i class="fas fa-caret-down pull-left" onclick="hideitem('{{$category->id}}');" style="cursor: pointer;"></i></h4>
					           	<ul style="display: none;" class="subcategorys" id="subcat_{{$category->id}}">
					           	@foreach($subcategories as $cat)
					           		@if($cat->pid == $category->id)
										
											<li><a href="/getsubcategoryproduct/{{ $cat->id}}"> {{ $cat->name}}</a>
											<!-- <a href="category/{{ $cat->id}}/subcategory" class="btn btn-xs pull-left" style="margin-top: -18px;">Edit</a> -->
											<!-- delete button -->
				              		 		{{ Form::open(['route' => ['category.destroy', $cat->id], 'method' => 'DELETE', 'id' => 'deleteform']) }} 
				                    			{!!  Form::submit('Delete',['class' =>'btn btn-none btn-xs pull-right', 'id' => 'deletebtn', 'style' => 'margin-top:-18px;']) !!} 
				                			{!! Form::close() !!}
				               				 <!-- delete button -->
				               				 </li>
										
										@endif
								@endforeach
								</ul>
				            </td>
				            
				            <td>
				            	<!-- delete button -->
				               <!--  {{ Form::open(['route' => ['category.destroy', $category->id], 'method' => 'DELETE'	]) }} 
				                    {!!  Form::submit('Delete',['class' =>'btn btn-danger btn-xs pull-left', 'id' => 'deletebtn']) !!} 
				                {!! Form::close() !!} -->
				                <!-- delete button -->
				          
				                <a href="{{ route('category.edit', $category->id)}}" class="btn btn-primary btn-xs">Edit</a>
				          		<!-- <a href="{{ route('category.show', $category->id)}}" class="btn btn-primary btn-success btn-xs">View</a>       -->
				            </td>
				        </tr>
			        @endif
		        @endforeach
		    </tbody>
		    <tfoot>
		         <tr>
		            <th class="th-sm">#
		            </th>
		            <th class="th-sm">Name
		            </th>
		            <th class="th-sm">Actions
		            </th>
		        </tr>
		    </tfoot>
		</table>
	</div>

	<div class="col-sm-4 col-md-4 col-lg-4">
		<div class="jumbotron" style="padding: 18px; border: 1px solid silver;background-color:white;">
			<p class="display-4">New Category</p>
			{!! Form::open(['route' => 'category.store', 'method' => 'POST']) !!}
			<div class="lead">
				{{ Form::text('name',null, ['class' => 'form-control'])}}
			</div>
			@if(!$categories->isEmpty() )
				<div class="lead">
					<ul style="list-style: none; font-size: 18px;">
						@foreach($categories as $category)
							@if($category->pid == 0)
									<li><input type="radio" value="{{ $category->id}}" name="category_id">{{ $category->name }}
										@foreach($subcategories as $cat)
												@if($cat->pid == $category->id)
													<ul  style="list-style: none;">
														<li>-{{ $cat->name}}</li>
													</ul>
												@endif
										@endforeach
									</li>
							@endif
						@endforeach
					</ul>
				</div>
			@endif
			
			<div class="lead">
				{{ Form::submit('Create',['class' => 'btn btn-success btn-block'])}}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<style>
	.subCategories{
		margin-left: 20px;
	}
</style>	
@endsection
@section('javascripts')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#dtBasicExample').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });

        function hideitem(id){
       		$('#subcat_'+id).toggle("slide");
        }

    </script>

	<script>
	    $(document).ready(function(){
	        $('#deletebtn').click(function(e){
	            if(confirm('Are you Sure you want to delete ?')){
	                $('#deleteform').submit();
	            }
	            else{
	                e.preventDefault();
	            }
	        e.preventDefault();
	        });
	    });
    </script>

<!--data table ends  -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
@endsection