@extends('layouts.app')
@section('title','| Edit Category') 
@section('pageheader','Category') 
@section('pageminiheader','Edit Category')
@section('navigate')
  <ol class="breadcrumb">
    <li><a href="{{route('category.index')}}"><i class="fa fa-bullseye"></i> Category</a></li>
    <li class="active">Edit Category</li>
  </ol>
@endsection 
@section('content')
  <!-- add product form -->
  <div class="col-md-7">
{!! Form::model($category, ['route' => ['category.update',$category->id],'method' =>'PUT', 'files' => 'true']) !!}
        <div class="form-group">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Category</h3>
        </div>
        
        <div class="box-body">
          <div class="form-group">
            {{ Form::label('name','Name') }}
            {{ Form::text('name',null,array('class' => 'form-control')) }}
          </div>

          <div class="form-group">
            {{ Form::label('image','Image') }}
            <a href="{{ asset('images/category/' .$category['image'] ) }}">            
                <img src="{{ asset('images/category/' . $category->image ) }}" alt="" class="img-fluid  product-image">            
            </a>
          </div>
          
          <div class="form-group">
            {{ Form::file('image', ['class'=>'form-control']) }}
          </div>

          <div class="form-group">
            <button type="submit" class="btn btn-primary">Update Category</button>
            <a href="{{ route('category.index')}}" class="btn btn-danger">Cancel</a>  
          </div>
        </div> 
      </div>
    </div>
  </div>
{!! Form::close() !!}

  <div class="col-md-5">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">
          Categories Under {{ $category->name }}
        </h3>
      </div>

      {!! Form::open(['route' =>'subcategory.update', 'method' => 'post']) !!}
      <div class="box-body">
        <div class="form-group">
          <input type="text" class="form-control" name="name" id="subcategorytext">
          <input type="hidden" value="" name="subcat_id" id="subcat_id">
        </div>
        <div class="form-group">
          
          <label for="" style="color: red;display: none;" id="label">
            Error : Select The Category To Edit  
          </label>
          
          @foreach($subcategories as $subcats)
          @if($subcats->pid == $category->id)
          <ul class="ul">
            <li>
              <p onclick="getcat('{{$subcats->name}}',{{$subcats->id}} )" style="cursor: pointer;">
                <i class="fas fa-caret-right"></i> 
                {{$subcats->name}} <i class="fas fa-pen" style="margin-left: 10px;"></i>
              </p>
              
              @foreach($subcategories as $subcats1)
              @if($subcats1->pid == $subcats->id)
                <ul class="ul">
                  <li>
                    <p onclick="getcat('{{$subcats1->name}}', {{ $subcats1->id}})" style="cursor: pointer;">
                      <i class="fas fa-caret-right"></i> {{ $subcats1->name }} <i class="fas fa-pen" style="margin-left: 10px;"></i>
                    </p>

                    @foreach($subcategories as $subcats2)
                    @if($subcats2->pid == $subcats1->id)
                      <ul class="ul">
                        <li>
                          <p onclick="getcat('{{$subcats2->name}}', {{ $subcats2->id }} )" style="cursor: pointer;">
                            <i class="fas fa-caret-right"></i> {{$subcats2->name}} <i class="fas fa-pen" style="margin-left: 10px;"></i>
                          </p>
                        </li>
                      </ul>
                    @endif
                    @endforeach

                  </li>
                </ul>
              @endif
              @endforeach

            </li>
          </ul>
          @endif
          @endforeach

        </div>
        <hr>
        <div class="form-group">
            <input type="submit" class="btn btn-success btn-md btn-block" onclick = "safesubmit(event)">     
          {!! Form::close(); !!}
          </form>
        </div>  
      
      </div>
    </div>
  </div>


<style> 
  .ul{
    color: #367fa9;
    list-style: none;
    font-size: 15px;
  }
  .product-image
  {
      border : 1px solid gray;
      height: 150px;
      width: auto;
      object-fit: contain;
  }
</style>

<script>
  function getcat($name,$id)
  {
    $('#subcategorytext').val($name);
    $('#subcat_id').val($id)
  }

  function safesubmit(e)
  {
      if($('#subcat_id').val() == ""){
          e.preventDefault();
          $('#label').show();
      }
      else{
          $(this).submit();
      }
  }
</script>

  <!-- form ends -->
@endsection
