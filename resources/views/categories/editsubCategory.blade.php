@extends('layouts.app')
@section('title','| Edit Category') 
@section('pageheader','Category') 
@section('pageminiheader','Edit Category') 
@section('content')
  <!-- add product form -->
  {!! Form::model($subcategory, ['route' => ['category.update',$subcategory->id],'method' =>'PUT']) !!}
    <div class="form-group">
      {{ Form::label('name','Name') }}
      {{ Form::text('name',null,array('class' => 'form-control')) }}
        
      <div class="col-lg-offset-2 col-lg-10" style="margin-top: 20px;">
            <button type="submit" class="btn btn-primary">Update Category</button>
            <a href="{{ route('category.index')}}" class="btn btn-danger">Cancel</a>  
      </div>

  {!! Form::close() !!}

  <!-- form ends -->
@endsection
