@extends('layouts.app')
@section('title','| Categories')
@section('pageheader','Categories')
@section('pageminiheader','Category Lists')
@section('navigate')
  <ol class="breadcrumb">
    <li><a href="{{route('category.index')}}"><i class="fa fa-bullseye"></i> Category</a></li>
    <li class="active">Category Lists</li>
  </ol>
@endsection
@section('content')
<style type="text/css">
	.subcategorys{list-style: none;}
	.subcategorys li{margin-bottom: 5px;}
</style>

<style>
	.subCategories{
		margin-left: 20px;
	}
    
    .deletebtn{
    	padding: 3px 3px;
    	height: 20px;
    	margin-right: 6px;
        color: ;
        background: #fff;
        border: none;
        border-radius: 2px;
        margin-top: -18	px;
    }
    .subcategorys ul li{margin-bottom: 10px; border-bottom: 1px solid #ccc; padding: 20px}
</style>


<div class="row">
	<div class="col-sm-12 col-md-8 col-lg-8" style="margin-top: 5px;">
		<table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
		    <thead>
		        <tr>
		            <th class="th-sm">#
		            </th>
		            <th class="th-sm">Name
		            </th>
		            <th class="th-sm">Actions
		            </th>
		        </tr>
		    </thead>
		    <tbody>
		    	@php
            		$count = 1;
       			@endphp
		        @foreach($categories as $category)
			       	@if($category->pid == 0)
				        <tr>
				            <td>@php echo $count++ @endphp</td>
				            <td>
					           	<h4>
					           		<a href="{{ route('category.show', $category->id) }}">{{ $category->name}}</a> 
					           		<!-- <i class="fas fa-caret-down pull-right" onclick="hideitem('{{$category->id}}');" style="cursor: pointer;"></i> -->
					           	</h4>
					           	<ul class="subcategorys" id="subcat_{{$category->id}}">
					           	@foreach($categories as $cat)
					           		@if($cat->pid == $category->id)
										
											<li>
												<a href="{{ route('category.show', $cat->id) }}"> 
													<i class="fas fa-caret-right" style="color: #3c8dbc;"></i> {{ $cat->name }}
												</a>
												
												<a href="{{ route('category.edit', $cat->id)}}" class="">
													<i class="fas fa-pen pull-right" title="edit"></i> 
												</a>

												
												<!-- delete button -->
				              		 			{{ Form::open(['route' => ['category.destroy', $cat->id], 'method' => 'DELETE', 'id' => 'deleteform', 'style' => 'margin-left: 453px; margin-top: -23px;' , 'onclick' => 'deletecategory(event)']) }} 
                        							<button type="submit" id="deletebtn" class="deletebtn "><i class="fas fa-trash-alt" title="delete"></i></button>
				                				{!! Form::close() !!}
				                				<!-- delete -->
				               			    </li>

												<!-- sub sub categories -->
												@foreach($categories as $subcat1)
												@if($subcat1->pid == $cat->id)
												<ul style="list-style: none;">
													<li class="row">
														<a href="{{ route('category.show', $subcat1->id) }}"> <i class="fas fa-caret-right" style="color: #3c8dbc;"></i> {{ $subcat1->name}}
														</a>
														<a href="{{ route('category.edit', $subcat1->id)}}" class="">
															<i class="fas fa-pen pull-right" title="edit"></i> 
														</a>	
														
							              		 		{{ Form::open(['route' => ['category.destroy', $subcat1->id], 'method' => 'DELETE', 'id' => 'deleteform', 'style' => 'margin-left: 453px; margin-top: -23px;' ,  'onclick' => 'deletecategory(event)']) }} 
			                        						<button type="submit" id="deletebtn" class="deletebtn pull-right"><i class="fas fa-trash-alt" title="delete"></i></button>
							                			{!! Form::close() !!}
							                			
							                			<ul style="list-style: none;">
														@foreach($categories as $subcat2)
														@if($subcat2->pid == $subcat1->id)
														
															<li>
																<a href="{{ route('category.show', $subcat2->id) }}"> <i class="fas fa-caret-right" style="color: #3c8dbc;"></i> {{ $subcat2->name}}</a>

																<a href="{{ route('category.edit', $subcat1->id)}}" class=""><i class="fas fa-pen pull-right" title="edit"></i> </a>	
																
									              		 		{{ Form::open(['route' => ['category.destroy', $subcat1->id], 'method' => 'DELETE', 'id' => 'deleteform',  'style' => 'margin-left: 439px; margin-top: -23px;' ,   'onclick' => 'deletecategory(event)']) }} 
					                        						<button type="submit" id="deletebtn" class="deletebtn pull-right"><i class="fas fa-trash-alt" title="delete"></i></button>
									                			{!! Form::close() !!}
									                			
															</li>
														
														@endif
							               			   	@endforeach
							               			   	</ul>

													</li>
												</ul>
												@endif
					               			   	@endforeach
										@endif
								@endforeach
								</ul>
				            </td>
				            
				            <td>
				            	<!-- delete button -->
				                {{ Form::open(['route' => ['category.destroy', $category->id], 'method' => 'DELETE', 'id' => 'deleteform',  'onclick' => 'deletecategory(event)']) }} 
		    						<button type="submit" id="deletebtn" class="deletebtn pull-left" style="margin-top: 1px; color: red;"><i class="fas fa-trash-alt" title="delete"></i></button>
							    {!! Form::close() !!}
				                <!-- delete button -->
				          
				                <a href="{{ route('category.edit', $category->id)}}" class="btn-md"><i class="fas fa-pen" title="edit" style="margin-top: 5px; margin-left: 5px;"></i> </a>
				          		<!-- <a href="{{ route('category.show', $category->id)}}" class="btn btn-primary btn-success btn-xs">View</a>       -->
				            </td>
				        </tr>
			        @endif
		        @endforeach
		    </tbody>
		    <!-- <tfoot>
		         <tr>
		            <th class="th-sm">#
		            </th>
		            <th class="th-sm">Name
		            </th>
		            <th class="th-sm">Actions
		            </th>
		        </tr>
		    </tfoot> -->
		</table>
	</div>
	
	<div class="col-md-4 col-sm-12" style="margin-top: 5px;">
		{!! Form::open(['route' => 'category.store', 'method' => 'POST', 'files' => 'true']) !!}
		<div class="box box-primary">
			<div class="box-header with-border">
        		<h3 class="box-title">New Category</h3>
      		</div>
      		<div class="box-body">
      			<div class="form-group">
						<input type="text" name="name" class="form-control" placeholder="name">
				</div>
				<div class="form-group">
		          {{Form::label('image','Image') }}
		          {{ Form::file('image', ['class'=>'form-control']) }}
		        </div>
      		</div>
		</div>

		<div class="box box-primary">
			<div class="box-header with-border">
				<div class="box-title">
					Select Parent
				</div>
			</div>
			<div class="box-body">
				<div class="form-group">
					@foreach($categories as $category)
					@if($category->pid == 0)
					<div class="radio">
						<label for="">
							<input type="radio" value="{{ $category->id}}" name="category_id"> <p style="font-size: 16px; margin: 0px;">{{ $category->name }}</p>
						</label>
					</div>
						@foreach($categories as $cat)
						@if($cat->pid == $category->id)
						<ul style="list-style: none;">
							<li>
								<div class="radio">
									<label for="">
										<input type="radio" value="{{ $cat->id}}"  name="subcategory_id" > <p style="font-size: 14px; margin: 0px;">{{ $cat->name}}</p>
									</label>
								</div>
							</li>
						
							<ul style="list-style: none;">
								<li>
									@foreach($categories as $sub1cat)	
									@if($sub1cat->pid == $cat->id)
									<div class="radio">
										<label for="">
											<input type="radio" value="{{ $sub1cat->id}}"  name="subcategory1_id" > <p style="font-size: 14px; margin: 0px;">{{ $sub1cat->name}}</p>
										</label>
									</div>
									<ul style="list-style: none;">
										<li>
											@foreach($categories as $sub2cat)
											@if($sub2cat->pid == $sub1cat->id)
												<div class="radio">	
													<label for="">
														<i class="fas fa-caret-right"></i> {{$sub2cat->name}}  
														<!-- <a href="{{ route('category.edit', $sub2cat->id)}}"> 
															<i class="fas fa-pen" style="margin-left: 55px; color : black;"></i>	 
														</a>
														<i class="fas fa-trash-alt" style="margin-left: 10px;"></i>
														<hr style="margin-top: 3px; margin-bottom: 0px;"> -->
													</label>
												</div>
											@endif
											@endforeach
										</li>
									</ul>
									@endif
									@endforeach
								</li>
							</ul>
						</ul>
						@endif
						@endforeach
						<hr style="margin-top: 3px; margin-bottom: 0px;">							
					@endif
					@endforeach

					{{ Form::submit('Create',['class' => 'btn btn-success btn-block btn-sm'])}}
				</div>
			</div>
		</div>
		{!! Form::close() !!}	
	</div>	
</div>
	
@endsection
@section('javascripts')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#dtBasicExample').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });

        // function hideitem(id){
       	// 	$('#subcat_'+id).toggle("slide");
        // }

    </script>
	<script>
    function deletecategory(e){
            if(confirm('Are you Sure you want to delete ?')){
                $(this).submit();
            }
            else{
                e.preventDefault();
            }
        }
    </script>
	<!-- <script>
	    $(document).ready(function(){
	        $('#deletebtn').click(function(e){
	            if(confirm('Are you Sure you want to delete ?')){
	                $('#deleteform').submit();
	            }
	            else{
	                e.preventDefault();
	            }
	        e.preventDefault();
	        });
	    });
    </script> -->

<!--data table ends  -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
@endsection