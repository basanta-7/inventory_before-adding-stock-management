
@extends('layouts.app')
@section('title',"| $category->name") 
@section('pageheader','Products') 
@section('pageminiheader', "found in $category->name category" )
@section('navigate')
  <ol class="breadcrumb">
    <li><a href="{{route('product.index')}}"><i class="fas fa-box-open"></i> Product</a></li>
    <li class="active">View Products</li>
  </ol>
@endsection 
@section('content')
<div class="col-md-12">
    <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="th-sm">#
                </th>
                <th class="th-sm">Name
                </th>
                <th class="th-sm">Rate
                </th>
                <th class="th-sm">Quantity
                </th>
                <th class="th-sm">Category
                </th>
                <th class="th-sm">Description
                </th>
                <th class="th-sm">Image
                </th>
                <th class="th-sm">Purchase Date
                </th>
                <th class="th-sm">Actions
                </th>
            </tr>
        </thead>
        <tbody>

            @php
                $count = 1;
            @endphp
            @foreach($category->product as $product)
            <tr>
                <td>@php echo $count++ @endphp</td>
                <td>{{ $product['name']}}</td>
                <td>{{ $product['rate']}}</td>
                <td>{{ $product['quantity']}}</td>
                <td>
                    @foreach($product->category as $cats)
                    <a href="{{ route('category.show', $cats->id) }}">
                        <span class="badge badge-primary" style="background-color: #6196ed; height: 20px; margin-top: 5px;"> {{ $cats->name }} </span>
                    </a>
                    <br>
                    @endforeach
                </td>
                <td>{!! substr($product['description'],0,20) !!}{{ (strlen($product['description']))>10 ? '....' : '' }}</td>
                <td style="text-align: center;">
                    <a href="{{ asset('images/' .$product['image'] ) }}">
                        <img src="{{ asset('images/' . $product['image'] .'-thumbs.png') }}" alt="" class="">    
                    </a>            
                </td>
                <td>{{ date('M j, Y h:ia',strtotime( $product['created_at'])) }}</td>
                <td>                    
                    <a href="{{ route('product.show', $product['slug'])}}"><i class="fas fa-eye pull-left" style="margin-right: 10px;" title="view" ></i></a>
                    <a href="{{ route('product.edit', $product['id'])}}"> <i class="fas fa-pen pull-left" style="margin-right: 10px;" title="edit"></i></a>
                    <!-- delete button -->
                    {{ Form::open(['route' => ['product.destroy', $product['id']], 'method' => 'DELETE', 'id' => 'deleteform', 'class' =>'pull-left','onclick' => 'deleteproduct(event)']) }} 
                        <!-- {!!  Form::submit('Delete',['class' =>'btn btn-xs delelteprod','style' =>'margin-top : 0px', 'id' => 'deletebtn']) !!}  -->
                        <button type="submit" id="deletebtn" class="deleteprod"><i class="fas fa-trash-alt pull-left" title="delete" ></i></button>
                    {!! Form::close() !!}
                    <!-- delete button -->

                </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Name
                </th>
                <th>Rate
                </th>
                <th>Quantity
                </th>
                <th>Category
                </th>
                <th>Description
                </th>
                <th>Image
                </th>
                <th>Purchase Date
                </th>
                <th>Actions
                </th>
            </tr>
        </tfoot>
    </table>

    <style>
          .deleteprod{
            color: red;
            background: none;
            border: none;
          }      
    </style>

</div>
@endsection
@section('javascripts')
    
    
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#dtBasicExample').DataTable();
            $('.dataTables_length').addClass('bs-select');     
        });
    </script>

    <script>
    function deleteproduct(e){
            if(confirm('Are you Sure you want to delete ?')){
                $(this).submit();
            }
            else{
                e.preventDefault();
            }
        }
    </script>

<!--data table ends  -->
@endsection

