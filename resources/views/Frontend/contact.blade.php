@extends('Frontend/layouts.app')
@section('title','Contact Us')
@section('content')
<div class="content">       
        <div class="row contactus">
            <div class="col-md-10 offset-md-1 paragraph">
                <h4>WE ARE HERE TO HELP YOU</h4>
                <hr>
                <p>Are you curious about something? Do you have some kind of problem with our products? As am hastily invited settled at limited civilly fortune me. Really spring in extent an by. Judge but built party world. Of so am he remember although required. Bachelor unpacked be advanced at. Confined in declared marianne is vicinity.</p>
                <p><small>Please feel free to contact us, our customer service center is working for you 24/7.</small></p>
            </div>
        </div>

        <div class="row icondiv">
            <div class="col-md-3 icons offset-md-1">
                <i class="fas fa-map-marker-alt" style="font-size: 70px; color: black;"></i>
                <h5>Address</h5>
                <p>13/25 Avenue</p>
                <p>New South Wales, 45Y73J</p>
                <p>England,Great Britain</p>
            </div>
            <div class="col-md-3 offset-md-0 icons">
                <i class="fas fa-phone-square" style="font-size: 70px; color: black;"></i>
                <h5>Call Us</h5>
                <p>This number is toll free if calling from Great Britain otherwise we advise you to use the electronic communication</p>
                <h6>+35 35566554</h6>
            </div>
            <div class="col-md-3 offset-md-0 icons">
                <i class="far fa-envelope" style="font-size: 70px; color: black;"></i>
                <h5>Electronic Support</h5>
                <p>Please feel free to write an email to us or to use our electronic ticketing system.</p>
                <h6>info@fakeemail.com</h6>
            </div>

        </div>
        <hr>

        <div class="row">    
            <div class="col-md-12  contact-form">
                <p class="contactheader text-center" style="color: #fff;">Send Us A Message</p>
                <p class="text-center" style="color: white; font-size: 14px;">Got a question? We'd love to hear from you. Send us a message below and we'll be sure to respond as soon as possible. </p>
                <form action="{{ url('contact-us') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" class="form-control col-md-6 offset-md-3" placeholder="Fullname*" name="name">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control col-md-6 offset-md-3" placeholder="Email*" name="email">
                    </div>    
                    <div class="form-group">
                        <input type="text" class="form-control col-md-6 offset-md-3" placeholder="Subject*" name="subject">
                    </div>
                    <div class="form-group">    
                        <textarea id="" cols="20" rows="8" class="form-control col-md-6 offset-md-3" placeholder="Message*" name="message"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger col-md-6 ">Send</button>
                    </div>
                </form>
            </div>
        </div>
</div>

<style>
    .contact-form .contactheader{
        color: black;
        margin-top: -10px;
        font-size: 22px;
        font-weight: 400;
    }
    .contact-form{
        
        text-align: center;
        background: #013f7e;
        padding: 20px;
    }
    .contact-form>p{
        color: white;
    }
    .btn-danger
    {
        background-color: #ff2d2d;
        color: white;
    }
    .form-group>input
    {
        border-radius: 0px;
    }    
    .form-group>textarea
    {
        border-radius: 0;
    }
    .form-group>button
    {
        border-radius: 0;
    }
    .form-group>p{
        color: black;
        font-size: 14px;
    }
    .contacts{
        margin-bottom: 10px;
        padding: 10px;
        background-color: #4848fc;
        color: black;
    }
    .contacts>p{
        color: white;
    }
    
    .paragraph>p{
        color: black;
    }

    .icons{
        margin-top: 5px;
        text-align: center;
    }
    .icons>h5{
        color: black;
        margin-top: 10px;
    }
    .icons>h6{
        color: green;
    }
    .icons>p
    {
        color: black;
        font-size: 15px;
    }
    .icondiv{
        margin-top: 40px;
    }
</style>
@endsection