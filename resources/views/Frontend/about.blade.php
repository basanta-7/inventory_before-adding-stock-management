@extends('Frontend/layouts.app')
@section('title','About Us')
@section('content')
<div class="content">
    <!-- <nav aria-label="breadcrumb" style="margin-left: -14px; margin-top: -30px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page" style="color: black;" >About Us</li>
      </ol>
    </nav> -->
      
        <div class="row">
            <div class="col-md-7 offset-md-1 about">
                <h4>About Us</h4>
                <p class="lead">
                    {!!$contents->content!!}
                </p>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="aboutimg">
                    <img src="{{ asset('images/pages/'.$contents->image) }}" alt="" class="img-responsive">
                </div>
            </div>
        </div>

</div>

<style>
    .about{
        padding: 5px;
        margin-bottom: 15px;
    }
    .about .lead{
        color: black;
        font-size: 15px;
    }
    .aboutimg{
        margin-top: 35px;
        margin-bottom: 15px;
    }
    .aboutimg >img{
        object-fit: contain;
        height: 390px;
        width: auto;
    }
</style>
@endsection