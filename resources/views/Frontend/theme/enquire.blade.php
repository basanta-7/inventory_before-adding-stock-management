@extends('Frontend\theme\layouts.app')
@section('title','Enquire')
@section('content')
<!-- Head END -->

<!-- Body BEGIN -->
<body class="ecommerce">
    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}">Home</a></li>
            <!-- <li><a href="{{url('/about')}}">About Us</a></li> -->
            <li class="active">Enquire</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar col-md-3 col-sm-3">
            <div class="pi-img-wrapper" style="border: 1px solid silver; padding: 5px 5px;">
              <img src="{{ asset('images/' . $products['image'] ) }}" class="img-responsive" alt="{{$products->name}}">                
            </div>
          </div>
          <!-- END SIDEBAR -->

          <!-- BEGIN CONTENT -->
          <div class="col-md-9 col-sm-7">
            <h1>Enquiry about our product</h1>
            <p>Interested in <b>{{$products->name}}</b> ? please fill the details below and we'll be sure to respond as soon as possible.</p>
            <div class="content-form-page">
              <form action="{{ route('enquire.store') }}" method="POST" class="form-horizontal form-without-legend">
                {{ csrf_field() }}
                <input type="hidden" value='{{ $products->id }}' name="prodid">                
                <div class="form-group">
                  <label class="col-lg-2 control-label" for="first-name">Full Name <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" name="name" id="first-name" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label" for="">Address<span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text"  name="address" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label" for="email">E-Mail Address<span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="email" name="email" id="email" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label" for="telephone">Telephone <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" id="telephone" name="contact" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-2 control-label" for="message">Queries</label>
                  <div class="col-lg-8">
                    <textarea class="form-control" rows="6" name="queries"></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-8 col-md-offset-2 padding-left-0 padding-top-20">
                    <button class="btn btn-primary" type="submit">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>
    <![endif]-->  
   <script src="{{asset('assets/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
    <script src="{{asset('assets/plugins/owl.carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
    <script src="{{asset('assets/plugins/zoom/jquery.zoom.min.js')}}" type="text/javascript"></script><!-- product zoom -->
    <script src="{{asset('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script><!-- Quantity -->
    <script src="{{asset('assets/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/rateit/src/jquery.rateit.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/corporate/scripts/layout.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();    
            Layout.initOWL();
            Layout.initTwitter();
            Layout.initImageZoom();
            Layout.initTouchspin();
            Layout.initUniform();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
@endsection
<!-- END BODY -->
</html>