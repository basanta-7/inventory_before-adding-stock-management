<div class="sidebar col-md-3 col-sm-5">
  <ul class="menubar">
    @foreach($categories as $cats)
    @if($cats->pid == 0)
    
    <li><a href="{{ route('item.getproducts', ['category' => $cats->slug]) }}">{{$cats->name}}</a>
      <ul class="menubar1">
        @foreach($categories as $cats1)
        @if($cats->id == $cats1->pid)
        <li><a href="{{ route('item.getproducts', ['category' => $cats1->slug]) }}">{{$cats1->name}}</a>
          <ul class="menubar2">
            @foreach($categories as $cats2)
            @if($cats1->id == $cats2->pid)
            <li><a href="{{ route('item.getproducts', ['category' => $cats2->slug]) }}">{{$cats2->name}}</a>
              <ul class="menubar3">
                @foreach($categories as $cats3)
                @if($cats2->id == $cats3->pid)
                <li><a href="{{ route('item.getproducts', ['category' => $cats3->slug]) }}">{{$cats3->name}}</a></li>
                @endif
                @endforeach
              </ul>
            </li>
            @endif
            @endforeach
          </ul>
        </li>
        @endif
        @endforeach
      </ul>
    </li>
    @endif
    @endforeach
  </ul>
</div>

<style>
  .menubar{
    /*border: 1px solid silver;*/
    background-color: #fff;
    padding: 10px;
  }
  .menubar li{
    margin-top: 10px;
    list-style: none;
    /*border-bottom: 1px solid silver;*/
  }
  .menubar1{
    display: none;
  }
  .menubar li:hover .menubar1{
    display: block;
  }
  .menubar2{
    display: none;
  }
  .menubar1 li:hover .menubar2{
    display: block;
  }
  .menubar3{
    display: none;
  }
  .menubar2 li:hover .menubar3{
    display: block;
  }

</style>

<!-- <div class="sidebar col-md-3 col-sm-5">
      <ul class="list-group margin-bottom-25 sidebar-menu">
        @foreach($categories as $cats)
        @if($cats->pid == 0)
        <li class="list-group-item clearfix dropdown active">
          <a href="javascript:void(0);" class="collapsed">
            <i class="fa fa-angle-right"></i>
            {{$cats->name}}
            
          </a>
          <ul class="dropdown-menu" style="display:block;">
            @foreach($categories as $cats1)
            @if($cats->id == $cats1->pid)
            <li class="list-group-item dropdown clearfix active">
              <a href="javascript:void(0);" class="collapsed"><i class="fa fa-angle-right"></i> {{$cats1->name}} </a>
                <ul class="dropdown-menu" style="display:block;">
                  @foreach($categories as $cats2)
                  @if($cats1->id == $cats2->pid)
                  <li class="list-group-item dropdown clearfix">
                    <a href="javascript:void(0);"><i class="fa fa-angle-right"></i> {{$cats2->name}} </a>
                    <ul class="dropdown-menu">
                      @foreach($categories as $cats3)
                      @if($cats2->id == $cats3->pid)
                      <li><a href="{{ route('item.getproducts', ['category' => $cats3->slug]) }}"><i class="fa fa-angle-right"></i> {{$cats3->name}}</a>
                          @foreach($categories as $cats4)
                          @if($cats3->id == $cats4->pid)
                          <li><a href="{{ route('item.getproducts', ['category' => $cats3->slug]) }}"><i class="fa fa-angle-right"></i> {{$cats4->name}}</a></li>
                          @endif
                          @endforeach
                      </li>
                      @endif
                      @endforeach
                    </ul>
                  </li>
                  @endif
                  @endforeach
                </ul>
            </li>
            @endif
            @endforeach
          </ul>
        </li>
        @endif
        @endforeach
      </ul>
    </div> -->