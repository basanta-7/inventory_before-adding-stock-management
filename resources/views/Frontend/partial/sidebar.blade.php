
<!-- Sidebar Holder -->
        <nav class="sidebar" id="sidebar">
            <div class="sidebar__header">
                <h3>Categories</h3>
            </div>
            <div class="searchbar">
                <form action="{{ url('/searchitem') }}" method='POST'>
                    {{ csrf_field() }}
                    <input class="search_input" type="text" name="searchtext" placeholder="Search...">
                    <button type="submit" class="search_icon" style="background: none; border: none;"><i class="fas fa-search"></i></button>
                </form>
            </div>

            <ul class="list-unstyled sidebar__components">
                <!-- <a href="#">Sale</a>
                <a href="#">Our Products</a>
                <a href="#">Lastest Products</a>
                <hr class="hr-only"> -->
                @foreach($categories as $cats)
                @if($cats->pid == 0)
                <li>
                    <a href="#electronicsSubmenu_{{ $cats->id }}" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle" onclick="hideitem('{{ $cats->id }}')">{{$cats->name}}</a>
                    <ul class="collapse list-unstyled" id="electronicsSubmenu_{{ $cats->id}}" style="background-color: #fff;">
                        @foreach($categories as $cats1)
                            @if($cats->id == $cats1->pid)
                                <li style="color: #007bff;">
                                    <a href="{{ route('item.getproducts', ['category' => $cats1->slug]) }}" style="font-size: 13px; margin-left: 20px;"><i class="fas fa-angle-right" style="color: #007bff;"></i> {{$cats1->name}}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </li>
                @endif
                @endforeach
            </ul>
        </nav>
        <script>
            function hideitem(id){
            $('#electronicsSubmenu__'+id).toggle("slide");
            }
        </script>