@if(Session::has('success'))
	
	<div class="alert alert-success" role="alert" style="margin-left: 26px; margin-right: 39px">
		<strong>Success:</strong> {{ Session::get('success') }}
	</div>

@endif

@if (count($errors) > 0)
    <div class="alert alert-danger" style="margin-left: 26px; margin-right: 39px">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif