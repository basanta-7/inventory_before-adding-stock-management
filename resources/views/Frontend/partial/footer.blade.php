<footer class="footer-info">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 logo-footer">
                <a href="#"><img src="{{asset('img/pandey.png')}}"></a>
                <div class="row logos">
                    <div><a href="#"><span class="fab fa-facebook"></span></a></div>
                    <div><a href="#"><span class="fab fa-youtube"></span></a></div>
                    <div><a href="#"><span class="fab fa-linkedin-in"></span></a></div>
                    <div><a href="#"><span class="fab fa-instagram"></span></a></div>
                </div>
            </div>
            <div class="col-lg-4 about">
                <h4>About</h4>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sit quis maxime quaerat quo
                    fugiat,
                    minima voluptatibus nihil recusandae quasi mollitia error aut ut itaque illum iste magni
                    pariatur modi. Possimus!</p>
            </div>

            <div class="col-lg-4 contact-info">
                <h4>Office</h4>
                <div class="address"><a href="#"><span class="fas fa-location-arrow"></span>Dhumbarahi,
                        Kathmandu,
                        Nepal</a></div>
                <div class="address"><a href="#"><span class="fas fa-phone"></span>01-400000 /
                        9851000000</a></div>
                <div class="address"><a href="#"><span class="fas fa-envelope"></span>hello@gmail.com</a>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="contaner-fluid">&#64; Copyrights Reserved.</div>
    </div>
</footer>