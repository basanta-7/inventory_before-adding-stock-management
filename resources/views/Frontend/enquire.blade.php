@extends('Frontend/layouts.main')
@section('title','Product Enquiry')
@section('content')
<div class="content">     
    <div class="row">
        <div class="col-md-12">
            <div class="background">
                <div class="col-md-8 offset-md-2">
                    <div class="enquiryform">
                        @foreach($products as $prods)
                            <h3 class="text-center">Inquiry about our product</h3>
                            <p class="text-center" style="color: black; font-size: 14px;">Interested in <b>{{ $prods->name }} ? </b> please fill the details below and we'll be sure to respond as soon as possible. </p>
                        @endforeach

                        <form action="{{ URL('enquire') }}" method="POST">
                            {{ csrf_field() }}
                            <label for="" class="col-md-3 offset-md-1">Fullname</label>
                            <input type="text" class="col-md-7" name="name">
                            
                            @foreach($products as $prods)
                                <!-- <input type="hidden" value='{{ $prods->name }}' name="prodname"> -->
                                <input type="hidden" value='{{ $prods->id }}' name="prodid">
                            @endforeach
    
                            <label for="" class="col-md-3 offset-md-1">Address</label>
                            <input type="text" class="col-md-7" name="address">


                            <label for="" class="col-md-3 offset-md-1">Email Address</label>
                            <input type="email" class="col-md-7" name="email">


                            <label for="" class="col-md-3 offset-md-1">Contact Number</label>
                            <input type="text" class="col-md-7" name="contact">

                            <label for="" class="col-md-3 offset-md-1">Queries</label>
                            <textarea class="col-md-7" rows="3" style="margin-top: 25px;" name="queries" > </textarea>
                            
                            <button type="submit" class="btn btn-danger btn-block col-md-7 offset-md-4">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>

<style>
    .background{
        padding: 25px 10px;
        margin-bottom: 0px;
        margin-top: -40px;
        background: url('{{asset('/img/bg.png')}}');
        height: auto;
        width: 100%;
    }
    .enquiryform{
        padding: 30px 15px;
        background: #e5e6e8;
    }
    .enquiryform>p{
        color: black;
    }
    form >label
    {
        margin-top: 25px;
    } 
    .btn{
        border-radius: 0;
    }
    /*.prodimage>img{
        width: 200px;
        height: auto;
        object-fit: contain;
    }*/
</style>
@endsection