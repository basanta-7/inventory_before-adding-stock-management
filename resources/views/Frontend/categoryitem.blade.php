@extends('Frontend/layouts.app')
@section('title','Products')
@section('content')
<div class="content">
    <nav aria-label="breadcrumb" style="margin-left: -14px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page" style="color: black;"><i class="fas fa-box-open"></i>                  @foreach($categorylist as $cats)
            {{ $cats->name }}
        @endforeach
        </li>
      </ol>
    </nav>
    
    
    <ul class="row content__item" style="">
        @foreach($categorylist as $catlist)
        @foreach($subcategories as $subcats)
            @if($subcats->pid == $catlist->id)
                <a href="{{ route('item.getproducts',$subcats->slug) }}" class="badge badge-info" style="margin-left: 10px;">{{$subcats->name}} </a>
            @endif
        @endforeach
        @endforeach
    </ul>
    </a>
    

    <div class="row pb-4 padding">
        @foreach($categorylist as $cats)
            @foreach($cats->product as $prods)
            <div class="col-lg-3 col-md-5 card" style="text-align: center;">
                <img src="{{ asset('images/' . $prods['image'] ) }}" class="card-img-top" alt="">
                <div class="card-body">
                    <a href="{{ route('item.show', $prods->slug) }}" class="card-text" style="font-size: 17px;">{{substr($prods->name,0,20) }}
                    {{strlen($prods->name)>20 ?'....' : '' }}
                    <h6 style="color: orange; font-weight: 600; font-size: 20px;">Rs : {{ $prods->rate}} </h6>
                    <!-- <h6 style="font-size: 10px;">{!! substr($prods->description,0,30) !!}</h6> -->
                    <a href="{{route('item.show', $prods->slug)}}"><div class="btn btn-success btn-block">See More</div></a>
                </div>
            </div>
            
            @endforeach
        @endforeach
    </div>

    <!--  -->
</div>
@endsection