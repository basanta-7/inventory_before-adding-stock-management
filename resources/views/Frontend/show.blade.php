@extends('Frontend/layouts.app')
@section('title','About Product')
@section('content')
<div class="content" style="background-color: #fff;">
    <nav aria-label="breadcrumb" style="margin-left: -14px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page" style="color: black;" ><i class="far fa-file-alt"></i>  Product Description</li>
      </ol>
    </nav>
    <!-- <ul class="row content__item" style="display: none;">
        <li class="col-md-3 content__list" id="list_1"><a href="#" class="content__link">Product</a></li>
        <li class="col-md-3 content__list" id="list_2"><a href="#" class="content__link">Accessories</a>
        </li>
        <li class="col-md-3 content__list" id="list_3"><a href="#" class="content__link">Gadgets</a></li>
    </ul> -->

    <div class="row pb-4 padding">
        @foreach($products as $prods)
        <div class="col-lg-4 col-md-5 card" style="padding: 5px; ">
            
            
                <div class="enquiry">
                    <a href="{{ url('/enquire/'.$prods->slug) }}">
                        <p class=""><i class="far fa-question-circle"></i>  Product Enquiry</p>
                    </a>
                </div>
            
            <img src="{{ asset('images/' . $prods['image'] ) }}" class="card-img-top prodimg_{{ $prods['image'] }}" onclick="window.open(this.src)" alt="" style="width: auto; height: 400px; object-fit: contain; cursor: pointer;">

           <!--  <p style="color: black;" class="lead text-center">
                <b>Quick Contact</b>
            </p>
            <p class="text-center" style="font-size: 30px; color: #28a745; margin-top: -20px;">
                +977 9856435670
            </p> -->
        </div>

        <div class="col-lg-7 col-md-5">
            <div class="proddesc">   
            <h4>{{ $prods->name }}</h4>
            <h5>Rs : {{ $prods->rate }}</h5>
            <hr>
            <p> <h5>Details</h5>{!! $prods->description !!}</p>
            </div>
        </div>
        @endforeach
    </div>    
</div>

<style>
    .proddesc{
        font-size: 14px;
        height: auto;
        width: auto;
        padding: 5px 15px;
        background-color: #fff;
        /*border: 1px solid #e0e1e2;*/
        /*background-color: #e9ecef;*/
        border-radius: 5px;
    }
    .proddesc >p{
        font-style: none;
        color: black;
    }
    .enquiry{
        text-align: center;   
        border-radius: 5px;
        width: 100%;
        height: 50px;
        background: #28a745;
    }
    .enquiry>a>p{
        color: white;
        margin-top: 10px;
    }
    .enquiry:hover{
        background: #28a745;
        opacity: .9;

    }
</style>

@endsection