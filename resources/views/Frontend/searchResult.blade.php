@extends('Frontend/layouts.app')
@section('title','Result')
@section('content')
<div class="content">    
    <nav aria-label="breadcrumb" style="margin-left: -14px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page" style="color: black;"><i class="fas fa-box-open"></i> Products</li>
      </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="">
                    <!-- Sort By : 
                    <a href="/?type=desc"> | Descending </a>
                    <a href="/?type=asc"> | Ascending</a> -->
                    @if($products != null)
                    <form action="{{ route('item.index') }}" method="get" class="sortform">
                        Sort By : 
                        <select name="sortby" id="sortby">
                            <option value="default">Default</option>
                            <option value="asc">Price low to high</option>
                            <option value="desc">Price high to low</option>
                        </select>
                    </form>
                    @endif
                </label>
            </div>
        </div>
    </div>

    <ul class="row content__item" > 
        @foreach($subcategories as $subcats)
            <a href="{{ route('item.getSubCategoryProduct',$subcats->id)}}">
                <li class="col-md-3 content__list" id="list_1" class="content__link">{{$subcats->name}}</a></li>
            </a>
        @endforeach
    </ul>

    <div class="row pb-4 padding">
        @foreach($products as $prods)
        <div class="col-lg-3 col-md-5 card" style="text-align: center;">
            <img src="{{ asset('images/' . $prods['image'] ) }}" class="card-img-top" alt="Mac Book Pro">
            <div class="card-body">
                <a href="{{ route('item.show', $prods->slug) }}" class="card-text">{{ $prods->name }}</a>
                <h6 style="color: orange; font-weight: 600; font-size: 20px;">Rs : {{ $prods->rate}} </h6>
                <!-- <h6 style="font-size: 10px;">{!! substr($prods->description,0,30) !!}</h6> -->
                <a href="{{route('item.show', $prods->slug)}}"><div class="btn btn-success btn-block">See More</div>
            </div>
        </div>
        @endforeach
    </div>
        <div class="text-center" style="margin-left: 35%;">
        {!! $products -> links();  !!}
        </div>

</div>
@endsection