@extends('Frontend/layouts.app')
@section('title','Search Results')
@section('content')
<style>
    .pagination li{
        font-size: 20px;
        width: 30px;
        height: 30px;
        border-radius: 2px;
        color: white;
        margin: 0 5px;
        background-color: #4286f4;
    }
</style>
<div class="content">
    <nav aria-label="breadcrumb" style="margin-left: -14px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page" style="color: black;"><i class="fas fa-search"></i> {{$productcount}}</li>
      </ol>
    </nav>
    <!-- <ul class="row content__item" style="display: none;">
        <li class="col-md-3 content__list" id="list_1"><a href="#" class="content__link">Product</a></li>
        <li class="col-md-3 content__list" id="list_2"><a href="#" class="content__link">Accessories</a>
        </li>
        <li class="col-md-3 content__list" id="list_3"><a href="#" class="content__link">Gadgets</a></li>
    </ul> -->
    
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="">
                    <!-- Sort By : 
                    <a href="/?type=desc"> | Descending </a>
                    <a href="/?type=asc"> | Ascending</a> -->
                    @if($productcount != 0)
                    <form action="{{ route('item.getsearch') }}" method="get" class="sortform">
                        <input type="hidden" name="search" value='{{$searchquery}}'>
                        Sort By : 
                        <select name="sortby" id="sortby">
                            <option value="default">Default</option>
                            <option value="asc" {{ $select == 'asc' ? 'selected' : '' }}> Price low to high</option>
                            <option value="desc" {{ $select == 'desc' ? 'selected' : '' }}> Price high to low</option>
                        </select>
                    </form>
                    @endif
                </label>
            </div>
        </div>
    </div>

    <div class="row pb-4 padding">
        @foreach($products as $prods)
        <div class="col-lg-3 col-md-5 card" style="text-align: center;">
            <img src="{{ asset('images/' . $prods['image'] ) }}" class="card-img-top" alt="">
            <div class="card-body">
                <a href="{{ route('item.show', $prods->slug) }}" class="card-text" style="font-size: 17px;">{{substr($prods->name,0,15) }}
                {{strlen($prods->name)>15 ?'....' : '' }}
                <h6 style="color: orange; font-weight: 600; font-size: 20px;">Rs : {{ $prods->rate}} </h6>
                <!-- <h6 style="font-size: 10px;">{!! substr($prods->description,0,30) !!}</h6> -->
                <a href="{{ route('item.show', $prods->slug) }}"><div class="btn btn-success btn-block">See More</div></a>
            </div>
        </div>
        @endforeach
    </div>
    <div class="text-center" style="margin-left: 35%;">
        {!! $products ->appends(request()->input())->links();  !!}
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('select[name="sortby"]').on('change',function(){
            $('.sortform').submit();
            // var sortid = $(this).val();
        })
    });
</script>
@endsection