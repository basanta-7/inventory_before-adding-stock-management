
@extends('layouts.app')
@section('title','| View Enquiries') 
@section('pageheader','Enquiries') 
@section('pageminiheader',$header) 
@section('navigate')
  <ol class="breadcrumb">
    <li><a href="{{route('enquire.index')}}"><i class="fas fa-box-open"></i> Enquiries</a></li>
    <li class="active">View</li>
  </ol>
@endsection
@section('stylesheet')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
<div class="col-md-12">
 <div class="box">
    <div class="box-header">
      <h3 class="box-title"> {{$header}} </h3>
    </div>
    <div class="box-body">
      <table id="example2" class="table table-bordered table-hover">
        <thead>
        <tr>
          <th>#</th>
          <th>Customer Name</th>
          <th>Product Name</th>
          <!-- <th>Address</th>
          <th>Email</th>
          <th>Contact</th> -->
          <th>Query</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
          @php
          $count = 1;
          @endphp
          @foreach($enquiries as $enq)
          <tr>
              <td>@php echo $count++ @endphp</td>
              <td>{{ $enq['username']}}</td>
              <td>{{ $enq->product->name}}</td>
              <!-- <td>{{ $enq['address']}}</td>
              <td>{{ $enq['email']}}</td>
              <td>{{ $enq['contact'] }}</td> -->
              <td>{{ substr($enq['query'],0,80) }} {{ (strlen($enq['query']))>80 ? '....' : '' }}</td>
              <td>
                  {{ Form::open(['route' => ['enquire.destroy', $enq['id']], 'method' => 'DELETE', 'id' => 'deleteform', 'class' =>'pull-left','onclick' => 'deleteenquiry(event)']) }} 
                      <button type="submit" id="deletebtn" class="btn btn-danger btn-sm deleteprod"><i class="fas fa-trash-alt " title="delete"></i></button>
                  {!! Form::close() !!}
                  <a href="{{route('enquire.list', $enq->id)}}" class="btn btn-primary btn-sm" title="view" style="margin-left: 10px;"><i class="far fa-eye"></i></a>
              </td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
        <tr>
          <th>#</th>
          <th>Customer Name</th>
          <th>Product Name</th>
          
          <th>Query</th>
          <th>Actions</th>
        </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
@endsection
@section('javascripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script>
    function deleteenquiry(e){
            if(confirm('Are you Sure you want to delete ?')){
                $(this).submit();
            }
            else{
                e.preventDefault();
            }
        }
    </script>
@endsection
