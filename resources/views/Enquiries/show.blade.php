@extends('layouts.app')
@section('title','| View Enquiry') 
@section('pageheader','Enquiry') 
@section('pageminiheader','View ') 
@section('navigate')
  <ol class="breadcrumb">
    <li><a href="{{route('enquire.index')}}"><i class="fas fa-question"></i> Enquiry</a></li>
    <li class="active">View</li>
  </ol>
@endsection

@section('content')
<div class="col-md-3">
    <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Enquiries</h3>

          <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-pills nav-stacked">
            <li><a href="{{ route('enquire.index') }}"><i class="fa fa-inbox"></i> All Enquiries
              <!-- <span class="label label-primary pull-right">12</span></a></li> -->
            <li><a href="{{ route('enquire.index', ['type' => 'unseen']) }}"><i class="fa fa-envelope-o"></i> Unseen enquiry messages</a></li>
            <!-- <li><a href="#"><i class="fa fa-file-text-o"></i> Drafts</a></li> -->
            <!-- <li><a href="#"><i class="fa fa-filter"></i> Junk <span class="label label-warning pull-right">65</span></a>
            </li>
            <li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li> -->
          </ul>
        </div>
    </div>

    <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Product Image</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <img src="{{ asset('images/'.$enquiry->product->image) }}" alt="" class="img-fluid" style="object-fit: contain;height: 225px; width: auto;">
            </div>  
          </div>

          </div>
</div>

<div class="col-md-9">
    <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Read Enquiry Messsage</h3>

              <!-- <div class="box-tools pull-right">
                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Previous"><i class="fa fa-chevron-left"></i></a>
                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Next"><i class="fa fa-chevron-right"></i></a>
              </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3>Enquiry about <a href="{{route('product.show',$enquiry->product->slug)}}">{{ $enquiry->product->name }}</a> </h3>
                <h5>
                  <span class="mailbox-read-time pull-right">{{ date('M j, Y h:ia',strtotime( $enquiry['created_at'])) }}</span></h5>
              </div>
              <!-- /.mailbox-read-info -->
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                <p><b>{{ $enquiry->username }}, </b>
                    <br>
                    {{ $enquiry->query }}</p>
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            <!-- /.box-footer -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="button" class="btn btn-default"><i class="fas fa-phone"></i>  {{ $enquiry->contact }}</button>
                <button type="button" class="btn btn-default"><i class="fas fa-envelope-square"></i> {{ $enquiry->email }}</button>
                <button type="button" class="btn btn-default"><i class="fas fa-user"></i> {{ $enquiry->username }}</button>
                <button type="button" class="btn btn-default"><i class="fas fa-home"></i> {{ $enquiry->username }}</button>
              </div>
            {{ Form::open(['route' => ['enquire.destroy', $enquiry['id']], 'method' => 'DELETE', 'id' => 'deleteform']) }}
                  <button type="submit" class="btn btn-danger" onclick="deleteenquiry(event)"><i class="fa fa-trash-o"></i> Delete</button>
            {!! Form::close() !!}
                
            </div>
            <!-- /.box-footer -->
          </div>
</div>
    <style>
        .product-image
        {
            margin: 5px;
            height: 350px;
            width: auto;
            object-fit: contain;
        }
    
    </style>

@endsection

@section('javascripts')
    <script>
    $(document).ready(function(){
        $('#deletebtn').click(function(e){
            if(confirm('Are You Sure ??')){
                $('#deleteform').submit();
            }
            else{
                e.preventDefault();
            }
        e.preventDefault();
        });
    });
    </script>

    <script>
    function deleteenquiry(e){
            if(confirm('Are you Sure you want to delete ?')){
                $(this).submit();
            }
            else{
                e.preventDefault();
            }
        }
    </script>
@endsection

