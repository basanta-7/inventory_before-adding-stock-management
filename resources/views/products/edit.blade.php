@extends('layouts.app')
@section('title','| Edit Product') 
@section('pageheader','Product') 
@section('pageminiheader','Edit Product')
@section('navigate')
  <ol class="breadcrumb">
    <li><a href="{{route('product.index')}}"><i class="fas fa-box-open"></i> Product</a></li>
    <li class="active">Edit Product</li>
  </ol>
@endsection 
@section('content')
  {!! Form::model($product, ['route' => ['product.update',$product->id],'method' =>'PUT', 'files' => 'true']) !!}
  <div class="col-md-7">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Update Product</h3>
      </div>
      
      <div class="box-body">
        <div class="form-group">
          {{ Form::label('name','Name') }}
          {{ Form::text('name',null,array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
          {{Form::label('image','Image') }}
          {{ Form::file('image', ['class'=>'form-control']) }}
        </div>

        <div class="form-group">
          <img src="{{asset('images/' . $product->image.'-thumbs.png')}}" alt="">  
        </div>

        <div class="form-group">
          <div class="col-md-6">
            {{ Form::label('quantity','Quantity') }}
            {{ Form::number('quantity',null,array('class' => 'form-control')) }}
          </div>
        </div>
        
        <div class="form-group">
          <div class="col-md-6">
            {{ Form::label('rate','Rate') }}
            {{ Form::number('rate',null,array('class' => 'form-control')) }}
          </div>
        </div>
      </div>
    </div>
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Description</h3>
        </div>
      
        <div class="box-body">
          <div class="form-group">
            {{ Form::textarea('description', $value = null, ['class' => 'form-control', 'rows' => 3]) }}
          </div>
        </div>
      </div>
  </div>

  <div class="col-md-4">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Select Category</h3>
      </div>

      <div class="box-body">  
        <div class="form-group">
          @foreach($categories as $cats)
          @if($cats->pid == 0 )
            <div class="checkbox">
              <label>
                <input type="checkbox" value="{{$cats->id}}" name="category_id[]" {{ $product->category->contains($cats->id) ? 'checked' : ''}} >
                {{ $cats->name}}
              </label>

              <ul style="text-decoration: none; list-style: none;">
                @foreach($categories as $subcat)
                @if($subcat->pid == $cats->id)
                  <li>
                    <label>
                      <input type="checkbox"  value="{{$subcat->id}}" name="category_id[]" {{ $product->category->contains($subcat->id) ? 'checked' : ''}}> {{ $subcat->name}}
                    </label>
                  </li>
                  
                  @foreach($categories as $subcat1)
                  @if($subcat1->pid == $subcat->id)
                  <ul style="text-decoration: none; list-style: none;">
                    <li>
                      <label for="">
                        <input type="checkbox"  value="{{$subcat1->id}}" name="category_id[]" {{ $product->category->contains($subcat1->id) ? 'checked' : ''}}> {{ $subcat1->name}}
                      </label>
                      @foreach($categories as $subcat2)
                      @if($subcat2->pid == $subcat1->id)
                      <ul style="text-decoration: none; list-style: none;">
                        <li>
                          <label for="">
                            <input type="checkbox"  value="{{$subcat2->id}}" name="category_id[]" {{ $product->category->contains($subcat2->id) ? 'checked' : ''}}> {{ $subcat2->name}}
                          </label>
                        </li>
                      </ul>
                      @endif
                      @endforeach
                    </li>
                  </ul>
                  @endif
                  @endforeach

                @endif
                @endforeach
              </ul>
              <hr>
            </div>
          @endif
          @endforeach

          <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <!-- <button type="reset" class="btn btn-danger">Reset</button>   -->
          </div>
        </div>
      </div>
    </div>
  </div>

{!! Form::close() !!}

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
          <script>
              $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });


              $( document ).ready(function() {
                var category_id = $('select[name="category_id"]').val();
                var subcat = <?php echo $product->pid;?>;
                var subcat1 = <?php echo $product->pid1;?>;

                      if(category_id){
                          $.ajax({
                              url: '/product/fetch',
                              type: 'POST',
                              dataType : 'json',
                              data: {id:category_id, _token: '{{csrf_token()}}' , subcatid:subcat, subcatid1:subcat1},
                              success: function(response){
                                //var success = $.parseJSON(response);
                                console.log(response);
                              $('#subcategory_id').html();
                              $("#subcategory_id").html(response.subcat);
                              $('#subcategory1_id').html();
                              $("#subcategory1_id").html(response.subcat1);
                            }
                          })
                        }        
                });

              $('select[name="category_id"]').on('change',function(){
                    var category_id = $(this).val();
                    var subcat = <?php echo $product->pid;?>;
                    if(category_id){
                    $.ajax({
                        url: '/product/fetch',
                        type: 'POST',
                        dataType : 'json',
                        data: {id:category_id, _token: '{{csrf_token()}}', subcatid:subcat },
                        success: function(data){
                        $('#subcategory_id').html();
                        $("#subcategory_id").html(data);
                      }
                    })
                  }
                }) 

                  $('select[name="subcategory_id"]').on('change',function(){
                    var subcategory_id = $(this).val();
                    var subcat = <?php echo $product->pid1;?>;
                    if(subcategory_id){
                    $.ajax({
                        url: '/product/fetchsubcat1',
                        type: 'POST',
                        dataType : 'json',
                        data: {id:subcategory_id, _token: '{{csrf_token()}}', subcatid:subcat },
                        success: function(data){
                        $('#subcategory1_id').html();
                        $("#subcategory1_id").html(data);
                      }
                    })
                  }
                }) 
                
          </script>

          <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
          <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
          <script>
              $('textarea').ckeditor();
          </script>

  <!-- form ends -->
@endsection
