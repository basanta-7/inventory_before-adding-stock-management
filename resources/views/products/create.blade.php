@extends('layouts.app')
@section('title','| Add Product')
@section('pageheader','Product')
@section('pageminiheader','Add Product')
@section('navigate')
  <ol class="breadcrumb">
    <li><a href="{{route('product.index')}}"><i class="fas fa-box-open"></i> Product</a></li>
    <li class="active"><a href="{{route('product.create')}}">Add Product</li></a>
  </ol>
@endsection
@section('content')
{!! Form::open(array('route' => 'product.store','files' => 'true')) !!}
  <div class="col-md-7">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Add Product</h3>
      </div>
      
      <div class="box-body">
        <div class="form-group">
          {{ Form::label('name','Name') }}
          {{ Form::text('name',null,array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
          {{Form::label('image','Image') }}
          {{ Form::file('image', ['class'=>'form-control']) }}
        </div>

        <div class="form-group">
          <div class="col-md-6">
            {{ Form::label('quantity','Quantity') }}
            {{ Form::number('quantity',null,array('class' => 'form-control')) }}
          </div>
        </div>
        
        <div class="form-group">
          <div class="col-md-6">
            {{ Form::label('rate','Rate') }}
            {{ Form::number('rate',null,array('class' => 'form-control')) }}
          </div>
        </div>
      </div>
    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Description</h3>
        </div>
      
        <div class="box-body">
          <div class="form-group">
            {{ Form::textarea('description', $value = null, ['class' => 'form-control', 'rows' => 3]) }}
          </div>
        </div>
      </div>
  </div>
  
  <div class="col-md-5">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Select Category</h3>
      </div>

      <div class="box-body">  
        <div class="form-group">
          @foreach($categories as $cats)
          @if($cats->pid == 0 )
            <div class="checkbox">
              <label>
                <input type="checkbox" value="{{$cats->id}}" name="category_id[]">  {{ $cats->name}}
              </label>

              <ul style="text-decoration: none; list-style: none;">
                @foreach($categories as $subcat)
                @if($subcat->pid == $cats->id)
                  <li>
                    <label>
                      <input type="checkbox"  value="{{$subcat->id}}" name="category_id[]"> {{ $subcat->name}}
                    </label>
                  </li>
                  
                  @foreach($categories as $subcat1)
                  @if($subcat1->pid == $subcat->id)
                  <ul style="text-decoration: none; list-style: none;">
                    <li>
                      <label for="">
                        <input type="checkbox"  value="{{$subcat1->id}}" name="category_id[]"> {{ $subcat1->name}}
                      </label>
                      @foreach($categories as $subcat2)
                      @if($subcat2->pid == $subcat1->id)
                      <ul style="text-decoration: none; list-style: none;">
                        <li>
                          <label for="">
                            <input type="checkbox"  value="{{$subcat2->id}}" name="category_id[]"> {{ $subcat2->name}}
                          </label>
                        </li>
                      </ul>
                      @endif
                      @endforeach
                    </li>
                  </ul>
                  @endif
                  @endforeach

                @endif
                @endforeach
              </ul>
              <hr>
            </div>
          @endif
          @endforeach
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add Product</button>
            <button type="reset" class="btn btn-danger">Reset</button>  
        </div>
      </div>
    </div>
  </div>
  <!-- <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Category</h3>
      </div>

      <div class="box-body">        
        <div class="form-group">
          {{ Form::label('category_id','Category') }}
          <select name="category_id" id="" class="form-control">
            <option value=""> ------Select One-------</option>
            @foreach($categories as $category)
            <option value="{{ $category->id}}"> {{ $category->name }} </option>
            @endforeach
          </select>  
        </div>
        
        <div class="form-group">
          {{ Form::label('subcategory_id','Category') }}
          <select name="sub_category" id="sub_category" class="form-control">
            <option value=""></option>
          </select>
        </div>
        
        <div class="form-group">
          {{ Form::label('subcategory1_id','Category') }}
          <select name="sub_category1" id="sub_category1" class="form-control">
            <option value=""></option>
          </select>
        </div>
      </div>

    </div>
  </div> -->

{!! Form::close() !!}


         

          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
          <script>
              $.ajaxSetup({
                 headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });
                $('select[name="category_id"]').on('change',function(){
                    var category_id = $(this).val();
                    if(category_id){
                    $.ajax({
                        url: '/product/fetch',
                        type: 'POST',
                        dataType : 'json',
                        data: {id:category_id, _token: '{{csrf_token()}}' },
                        success: function(data){
                        $('#sub_category').html();
                        $("#sub_category").html(data);
                      }
                    })
                  }
                  $('#sub_category').empty();
                  $('#sub_category1').empty();                  
                })
                $('select[name="sub_category"]').on('change',function(){
                    var subcategory_id = $(this).val();
                    
                    if(subcategory_id){
                    $.ajax({
                        url: '/product/fetchsub',
                        type: 'POST',
                        dataType : 'json',
                        data: {id:subcategory_id, _token: '{{csrf_token()}}' },
                        success: function(data){
                        $('#sub_category1').html();
                        $("#sub_category1").html(data);
                      }
                    })
                  }
                })
          </script>

    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection
