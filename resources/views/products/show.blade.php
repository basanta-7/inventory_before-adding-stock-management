@extends('layouts.app')
@section('title','| View Products') 
@section('pageheader','Product') 
@section('pageminiheader','View Product') 
@section('navigate')
  <ol class="breadcrumb">
    <li><a href="{{route('product.index')}}"><i class="fas fa-box-open"></i> Product</a></li>
    <li class="active">View Product</li>
  </ol>
@endsection
@section('content')
    <div class="row" style="background-color: white;">
        @foreach($products as $product)
        <div class="col-md-4">
            <a href="{{ asset('images/' .$product['image'] ) }}">            
                <img src="{{ asset('images/' . $product->image ) }}" alt="" class="img-fluid product-image">            
            </a>
        </div>

        <div class="col-md-7 product-description">
            <h2 class="">{{ $product->name}}</h2>
            
            <p> <b id="rate">Rs : {{ $product->rate}} </b></p>
            <p> <b>Quantity :  </b>{{ $product->quantity}}</p>
            <p> 
                <b>Categories :  </b>
                @foreach($product->category as $cats)
                    <a href="{{ route('category.show', $cats->id) }}">
                        <span class="badge badge-primary" style="background-color: #6196ed; height: 20px;"> {{ $cats->name }} </span>
                    </a>
                @endforeach
            </p>
            <p> <b>Purchased Date  : </b>{{ date('M j, Y h:ia',strtotime( $product->created_at)) }} </p>
            <p style=""> <b>Description :  </b>{!! $product->description !!} </p>
            <p>
                <a href="{{route('product.edit', $product->id)}}" class="btn btn-default btn-sm"> Edit </a>
                {{ Form::open(['route' => ['product.destroy', $product->id], 'method' => 'DELETE', 'id' => 'deleteform']) }} 
                    {!! Form::submit('Delete',['class' =>'btn btn-danger btn-sm', 'id'=>'deletebtn']) !!} 
                {!! Form::close() !!}
            </p>
        </div>
        @endforeach
    </div>
    
    <style>
        .product-image
        {
            margin: 5px;
            height: 350px;
            width: auto;
            object-fit: contain;
        }
        .product-description>p{
            font-size: 17px;
        }
        .product-description>p #rate{
            font-size: 40px;
            color : orange;
        }
        .product-description>p>b{
            font-size: 17px;
        }
        .product-description>p>a{
            margin-top: 20px;
            font-size: 16px;
        }
        #deletebtn{
            margin-top: -46px;
            margin-left: 60px;
            color: white;
            font-size: 16px;
        }
    </style>

@endsection

@section('javascripts')
    <script>
    $(document).ready(function(){
        $('#deletebtn').click(function(e){
            if(confirm('Are You Sure ??')){
                $('#deleteform').submit();
            }
            else{
                e.preventDefault();
            }
        e.preventDefault();
        });
    });
    </script>
@endsection

