<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = [
		'name', 'rate', 'quantity', 'description', 'image', 'slug'
	];
	
	public function category(){
		return $this->belongsToMany('App\Category');
	}
	public function enquiry(){
		return $this->hasMany('App\Enquiry');
	}
}
