<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use App\Category;
use Image;
use Storage;
use DB;

class ProductController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return view('products.index')->withproducts($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all()->sortBy('name');
        return view('products.create')->withcategories($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validation
        $this->validate($request, array(
            'name' => 'required|max:255',
            'rate' => 'required|numeric',
            'quantity' => 'required|numeric',
            'category_id' => 'required',
            // 'sub_category' => 'integer',
            
            'description' => 'required|',
            'image' => 'required|image|max:4096'));
        // store
        $product = new Product;
        $product->name = $request->name;
        $product->rate = $request->rate;
        $product->quantity = $request->quantity;
        $product->slug = str_slug($request->name. '-' .rand(10,90)); //slug
        
        if($request->sub_category == null)
        {}
        else
        {
            $product->pid = $request->sub_category;
        }
        if($request->sub_category1 == null)
        {}
        else
        {
            $product->pid1 = $request->sub_category1;
        }
        $product->description = $request->description;

        // saving the image of product
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension(); //encode('png')
            $image = Image::make($image)->save(public_path('images/' . $filename));
            $image->fit(110,100)->save(public_path('images/' . $filename . '-thumbs.png'));

            $product->image = $filename;   
        } 
        $product->save();  

        $product->category()->sync($request->category_id, false);

        Session::flash('success','Product was successfully saved !');
        
        return redirect()->route('product.show',$product->slug);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        // validation
        $this->validate($request, array(
            'name' => 'required|max:255',
            'rate' => 'required|numeric',
            'quantity' => 'required|numeric',
            'category_id' => 'required',
            // 'subcategory_id' => 'required|integer',
            // 'subcategory1_id' => 'required|integer',
            'image' => 'image|max:4096',
            'description' => 'required|'));

        $product->name = $request->input('name');
        $product->rate = $request->input('rate');
        $product->quantity = $request->input('quantity');
        $product->slug = str_slug($request->name. '-' .rand(10,90)); //slug
        // $product->pid = $request->input('subcategory_id');
        // $product->pid1 = $request->input('subcategory1_id');
        $product->description = $request->input('description');

        if($request->hasFile('image'))
        {
            // add the new photo
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension(); 
            $image = Image::make($image)->save(public_path('images/' . $filename));
            $image->fit(110,100)->save(public_path('images/' . $filename . '-thumbs.png'));        
            $oldfilename = $product->image;
            $oldfilenamethumb = $product->image . '-thumbs.png';
            // update the database 
            $product->image = $filename;   
            // delete the old photo
            Storage::delete($oldfilename);
            Storage::delete($oldfilenamethumb);
        }
        $product->save();
        // set the flash message

        $product->category()->sync($request->category_id, true);

        Session::flash('success','Product was successfully updated !');
        // redrecting to show products
        return redirect()->route('product.show',$product->slug);
    }

    public function getSearch(Request $request){
        $product  = $request->get('searchtext');

        $products = Product::where('name', 'LIKE', '%'.$product.'%') 
                                    ->orWhere('rate', 'LIKE', '%' .$product. '%' )
                                    ->orWhere('quantity', 'LIKE', '%' .$product. '%' )
                                    ->orWhere('description', 'LIKE', '%' .$product. '%' )
                                    ->orWhere('slug', 'LIKE', '%' .$product. '%' )->get();

        return view('products.index')->withproducts($products);
        
        // else
        // {
        //     $categories = Category::where('name', 'LIKE', '%' .$product. '%')->get();
        //     $subcategories = Category::all();
        //     return view('categories.searchresult')->withcategories($categories)->with('subcategories',$subcategories);  
        // }
                
        // return view('products.search')->with('products',$result);   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product = Product::where('slug', '=', $slug)->get();
        return view('products.show')->withproducts($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        // for select form binding
        $categories = Category::all()->sortBy('name');
        // $cats = [];
        // foreach($categories as $category){
        //     $cats[$category->id] = $category->name;
        // }

        return view('products.edit')->withProduct($product)->withcategories($categories);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->category()->detach();
        Storage::delete($product->image . '-thumbs.png');
        Storage::delete($product->image);   
        $product->delete();

        Session::flash('success','Product deleted successfully');

        return redirect()->route('product.index');
    }

    public function destroysearch($id)
    {
        $product = Product::find($id);
        Storage::delete($product->image . '-thumbs.png');
        Storage::delete($product->image);
        $product->delete();

        Session::flash('success','Product deleted successfully');

        return redirect()->back();
    }

    // public function getCategoryProduct($id){
    //     $products = Product::where('category_id', '=', $id)->get();
    //     // foreach($products as $pro)
    //     //     dd($pro['slug']);

    //     $itemlist = array();
    //     $icount = 0;
    //     foreach($products as $item){
    //         $subcategory = $this->getSubCategory($item->pid);
    //         $itemlist[$icount]['id']=$item->id;
    //         $itemlist[$icount]['pid']=$item->pid;
    //         $itemlist[$icount]['name']=$item->name;
    //         $itemlist[$icount]['rate']=$item->rate;
    //         $itemlist[$icount]['slug']=$item->slug;
    //         $itemlist[$icount]['image']=$item->image;
    //         $itemlist[$icount]['quantity']=$item->quantity;
    //         $itemlist[$icount]['description']=$item->description;
    //         $itemlist[$icount]['category']=$item->category->name;
    //         $itemlist[$icount]['subcategory']=$subcategory;
    //         $itemlist[$icount]['created_at']=$item->created_at;
    //         $icount++;
    //     }
    //     return view('products.searchcategory')->with('itemlist',$itemlist);
    // }

    // public function getSubCategoryProduct($id)
    // {
    //     $products = Product::where('pid', '=' , $id)->get();
        
    //     $itemlist = array();
    //     $icount = 0;
    //     foreach($products as $item){
    //         $subcategory = $this->getSubCategory($item->pid);
    //         $itemlist[$icount]['id']=$item->id;
    //         $itemlist[$icount]['pid']=$item->pid;
    //         $itemlist[$icount]['name']=$item->name;
    //         $itemlist[$icount]['rate']=$item->rate;
    //         $itemlist[$icount]['slug']=$item->slug;
    //         $itemlist[$icount]['image']=$item->image;
    //         $itemlist[$icount]['quantity']=$item->quantity;
    //         $itemlist[$icount]['description']=$item->description;
    //         $itemlist[$icount]['category']=$item->category->name;
    //         $itemlist[$icount]['subcategory']=$subcategory;
    //         $itemlist[$icount]['created_at']=$item->created_at;
    //         $icount++;
    //     }
    //     return view('products.searchsubcategory')->with('itemlist',$itemlist);
    // }

    // public function getSubCategory1Product($id){
    //     $products = Product::where('pid1', '=', $id)->get();
    //     // foreach($products as $pro)
    //     //     dd($pro['slug']);

    //     $itemlist = array();
    //     $icount = 0;
    //     foreach($products as $item){
    //         $subcategory = $this->getSubCategory($item->pid);
    //         $itemlist[$icount]['id']=$item->id;
    //         $itemlist[$icount]['pid']=$item->pid;
    //         $itemlist[$icount]['name']=$item->name;
    //         $itemlist[$icount]['rate']=$item->rate;
    //         $itemlist[$icount]['slug']=$item->slug;
    //         $itemlist[$icount]['image']=$item->image;
    //         $itemlist[$icount]['quantity']=$item->quantity;
    //         $itemlist[$icount]['description']=$item->description;
    //         $itemlist[$icount]['category']=$item->category->name;
    //         $itemlist[$icount]['subcategory']=$subcategory;
    //         $itemlist[$icount]['created_at']=$item->created_at;
    //         $icount++;
    //     }
    //     return view('products.searchsubcategory1')->with('itemlist',$itemlist);
    // }

    // fetching using ajax
    // public function fetch(Request $request){
    //     $id = $request->id;
    //     $subid = $request->subcatid;
    //     $subcategorys = Category::where('pid','=',$id)->get();
    //     $output = '<option value="0">------Select One------</option>'; 
    //     foreach ($subcategorys as $key => $value) {
    //         if($subid == $value->id){
    //             $selected = 'selected';
    //         }
    //         else{
    //             $selected = '';
    //         }

    //         $output .='<option value="'.$value->id.'" '.$selected.'>'.$value->name.'</option>';
    //     }
    //     return response()->json($output);
    // }


    // // fetching using ajax
    // public function fetch1(Request $request){
    //     $id = $request->id;
    //     $subid = $request->subcatid;
    //     $subid1 = $request->subcatid1;
        
    //     $subcategorys = Category::where('pid','=',$id)->get();
    //     $subcategorys1 = Category::where('pid','=',$subid)->get();

    //     $output = '<option value="0">------Select One------</option>'; 
    //     $output1 = '<option value="0">------Select One------</option>'; 
    //     foreach ($subcategorys as $key => $value) {
    //         if($subid == $value->id){
    //             $selected = 'selected';
    //         }
    //         else{
    //             $selected = '';
    //         }

    //         $output .='<option value="'.$value->id.'" '.$selected.'>'.$value->name.'</option>';
    //     }

    //     foreach ($subcategorys1 as $key => $value) {
    //         if($subid1 == $value->id){
    //             $selected = 'selected';
    //         }
    //         else{
    //             $selected = '';
    //         }

    //         $output1 .='<option value="'.$value->id.'" '.$selected.'>'.$value->name.'</option>';
    //     }
    //     echo json_encode(array("subcat"=>$output, "subcat1"=>$output1));
    //    // return response()->json($output);
    // }

    // // fetching subcat using ajax
    // public function fetchsub(Request $request){

    //     $id = $request->id;

    //     $subid = $request->subcatid;
    //     $subcategorys = Category::where('pid','=',$id)->get();

    //     $output = '<option value="0">------Select One------</option>'; 
    //     foreach ($subcategorys as $key => $value) {
    //         if($subid == $value->id){
    //             $selected = 'selected';
    //         }
    //         else{
    //             $selected = '';
    //         }

    //         $output .='<option value="'.$value->id.'" '.$selected.'>'.$value->name.'</option>';
    //     }
    //     return response()->json($output);
    // }


    // // fetching subcategory1 using ajax
    // public function fetchsubcat1(Request $request){
    //     $id = $request->id;
    //     $subid = $request->subcatid;
    //     $subcategorys = Category::where('pid','=',$id)->get();
    //     $output = '<option value="0">------Select One------</option>'; 
    //     foreach ($subcategorys as $key => $value) {
    //         if($subid == $value->id){
    //             $selected = 'selected';
    //         }
    //         else{
    //             $selected = '';
    //         }

    //         $output .='<option value="'.$value->id.'" '.$selected.'>'.$value->name.'</option>';
    //     }
    //     return response()->json($output);
    // }


    // // getting sub categories
    // public static function getSubCategory($id){
       
    //     $subcategories = Category::where('id', $id)->first();
    //     return $subcategories['name'];
    // }

    // // getting sub pid1
    // public static function getSubCategory1($id){
       
    //     $subcategories = Category::where('id', $id)->first();
    //     return $subcategories['name'];
    // }
}
