<?php

namespace App\Http\Controllers;

use App\Category;
use app\Product;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\File;
use Session;

class CategoryController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all()->sortBy('name');
        return view('categories.index')->withcategories($categories);
    }

    public function editSubCategory($id){
        $subCategory = Category::find($id);
        // return redirect()->back()->with('subcategory',$subCategory);
        return view('categories.editsubCategory')->with('subcategory',$subCategory);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
                'image' => 'image|max:4096',
                'name'=>'required|max:255'));

        $category = new Category();

        $category->name = $request->name; 
        $category->slug = str_slug($request->name);
        
        if($request->subcategory1_id !=null)
        {
            $category->pid = $request->subcategory1_id;
        }
        elseif($request->category_id == null && $request->subcategory_id != null)
        {
            $category->pid = $request->subcategory_id;           
        }
        elseif($request->category_id == null){
            $category->pid = 0;
        }
        elseif($request->category_id != null && $request->subcategory_id != null)
        {
            $category->pid = $request->subcategory_id;
        }
        else{
            $category->pid = $request->category_id;
        }

        // saving the image of category
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension(); //encode('png')
            $image = Image::make($image)->save(public_path('images/category/' . $filename));
            // $image->fit(110,100)->save(public_path('images/' . $filename . '-thumbs.png'));

            $category->image = $filename;   
        }

        $category->status = 1;
        $category->save();

        Session::flash('success','New Category Created Successfully');

        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);
        return view('categories.show')->with('category',$category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategories = Category::all()->sortBy('name');
        $category = Category::find($id);
        return view('categories.edit')->with('category',$category)->with('subcategories',$subcategories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $this->validate($request, array(
                'image' => 'image|max:4096',
                'name'=>'required|max:255'));

        $category->name = $request->input('name');
        $category->slug = str_slug($request->name);

        if($request->hasFile('image'))
        {
            // add the new photo
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension(); 
            $image = Image::make($image)->save(public_path('images/category/' . $filename));
            // $image->fit(110,100)->save(public_path('images/' . $filename . '-thumbs.png'));        
            $oldfilename = $category->image;
            // $oldfilenamethumb = $category->image . '-thumbs.png';
            // update the database 
            $category->image = $filename;   
            // delete the old photo
            $path = public_path()."\images\category".'\\';
            // dd($path);
            File::delete($path.$oldfilename);
        }

        $category->save();

        Session::flash('success','Category Updated Successfully !!');

        return redirect()->back();  
    }
    public function updatesubcategory(Request $request)
    {
        $id = $request->subcat_id;
        $name = $request->name;
        
        $category = Category::find($id);
        $this->validate($request, array(
                'name'=>'required|max:255'));

        $category->name = $name;
        $category->slug = str_slug($name. '-' .rand(0,200));
        $category->save();

        Session::flash('success','Category Updated Successfully !!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategories = Category::where('pid', '=', $id);
        $subcategories->delete();
        
        $category = Category::find($id);
        $category->product()->detach();
        $path = public_path()."\images\category".'\\';
        // dd($path);
        File::delete($path.$category->image);
        // File::delete($path.$category->image);
        $category->delete();

        Session::flash('success','Category Deleted Successfully !!');
        return redirect()->route('category.index');
    }
}
