<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Image;
use Storage;

class PageController extends Controller
{
	// public function __construct(){
 //        $this->middleware('auth');
 //    }

    public function getDashboard(){
        // dd('not allowed');
    	return view('products.dashboard');
    }

    public function getAbout()
    {
        $contents = DB::Table('about')->first();
        // dd($contents);
        return view('pages.about')->with('contents',$contents); 
    }

     public function getContact()
    {
    	return view('pages.about');
    }

    public function postAbout(Request $request)
    {
        $this->validate($request,[
                        'content' => 'required|max:4000',
                        'image' => 'max:4096'
                    ]);

        $content = $request->input('content');
        $image = $request->input('image');

        //handle file upload
        // if($request->hasFile('image')){
        //     //get filename with extension
        //     $fileNameWithExt =$request->file('image')->getClientOriginalName();
        //     //get just filename
        //     $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
        //     //get just extension
        //     $extension = $request->file('image')->getClientOriginalExtension();
        //     //filename to store
        //     $fileNameToStore =$filename.'_'.time().'.'.$extension;
        //     //upload image

        //     $path = $request->file('image')->storeAs('public/images/pages/', $fileNameToStore);
        //     DB::table('about')->update(['content' => $content, 'image' => $fileNameToStore]);
        // }
        if($request->hasFile('image'))
        {
            // add the new photo
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension(); 
            $image = Image::make($image)->save(public_path('images/pages/' . $filename));
            // $image->fit(110,100)->save(public_path('images/' . $filename . '-thumbs.png'));        
            // $oldfilename = $category->image;
            // $oldfilenamethumb = $category->image . '-thumbs.png';
            // update the database 
            // $category->image = $filename;   
            // delete the old photo
            // $path = public_path()."\images\category".'\\';
            // dd($path);
            // File::delete($path.$oldfilename);
            DB::table('about')->update(['content' => $content, 'image' => $filename]);

        }
        else{
        DB::table('about')->update(['content' => $content]);
        }


        $contents = DB::Table('about')->first();

        Session::flash('success !!', 'Successfully Saved');
        return redirect()->route('page.getabout')->with('contents',$contents);
    }

     public function postContact()
    {
        return 'asdsada';
    }
}
