<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Redirect;

use App\User;
use Session;
class UserController extends Controller
{
	public function __construct(){
        $this->middleware('user',['except' => ['getProfile','updateInformation']]);
        $this->middleware('auth');
    }

    public function getProfile($id){
        $userinfo = User::find($id);
        return view('user.profile')->with('userinfo',$userinfo);
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        // validation
        $this->validate($request, array(
            'name' => 'required|min:2|max:50',
            'email' => 'required|email',
            'password' => 'required|min:5|max:20',
            'repassword' => 'required|min:5|max:20',
            'role' => 'required'));
        
        // store
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role = $request->role;	

        $user->save();  

        Session::flash('success','User was successfully created !');
        
        return redirect()->route('users.index');
    }

    public function index(){
    	$users = User::all();
    	return view('user.index')->with('users',$users);
    }


    public function edit($id)
    {
        $user = User::find($id);
        return view('user.edit')->with('user',$user);
    }

    public function destroy($id){
    	$user = User::find($id);
    	$user->delete();

    	Session::flash('success','User Removed Successfully');

    	return redirect()->route('users.index');
    }

    public function update(Request $request, $id){
    	$user = User::find($id);

    	$this->validate($request, array(
            'name' => 'required|min:2|max:50',
            'email' => 'required|email',
            'role' => 'required'));

    	$user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role = $request->input('role');
        $user->save();

    	Session::flash('success','User Updated Successfully');

    	return redirect()->route('users.index');
    }

    public function updateInformation(Request $request){
        $id = $request->input('userId');
        $user = User::find($id);

        $oldpwd = $request->input('oldpassword');
        if($oldpwd != null)
        {
            if(User::checkPassword($oldpwd, $user->password))
            {
                // dd('old password matches');
                $this->validate($request, array(
                    'name' => 'required|min:2|max:50',
                    'email' => 'required|email',
                    'password' => [
                    'required',
                    'string',
                    'min:6',             // must be at least 6 characters in length
                    // 'regex:/[a-z]/',      // must contain at least one lowercase letter
                    // 'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    // 'regex:/[0-9]/',      // must contain at least one digit
                    // 'regex:/[@$!%*#?&]/', // must contain a special character
                    ],
                    'confirm-password' => 'same:password'));


                $user->name = $request->input('name');
                $user->email = $request->input('email');

                if($user->password != null)
                {
                    $user->password = Hash::make($request->password);
                }

                $user->save();

                Session::flash('success','Profile Updated Successfully');

                return Redirect::back();
                
            } 
            else
            {
                // dd('old pw doesnt match');
            return Redirect::back()->withErrors('Old password doesnt match our records !!!');
            }
        }

        if($request->input('name') !=null && $request->input('email') !=null && $oldpwd == null && $request->input('password') !=null || $request->input('confirm-password') !=null){
            return Redirect::back()->withErrors('Please Enter Your Old Password First !!!');  
        }
        
        if($oldpwd == null)
        {    
            $this->validate($request, array(
                'name' => 'required|min:2|max:50',
                'email' => 'required|email'));

            $user->name = $request->input('name');
            $user->email = $request->input('email');

            $user->save();

            Session::flash('success','Email and Username Updated Successfully');
            return Redirect::back();
        }
        
    }
}
