<?php

namespace App\Http\Controllers;

use App\Enquiry;
use Illuminate\Http\Request;
use Session;
use App\Product;
class EnquiryController extends Controller
{
    public function __construct(){
        $this->middleware('auth',['except' => ['store','show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->type)
        {
            $enquiries = Enquiry::where('status','=',0)->get();
            $header = 'Unseen Enquiries';
            return view('Enquiries.index')->with('enquiries',$enquiries)->with('header',$header);      
        }
        else{
            $enquiries = Enquiry::all();
            $header = 'All Enquiries';
            return view('Enquiries.index')->with('enquiries',$enquiries)->with('header',$header);   
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$slug)
    {
        $products = Product::where('slug', '=', $slug )->get();
        return view('Frontend.enquire')->withProducts($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
                            'name' => 'required|min:2|max:50',
                            'address' => 'required|min:3|max:20',
                            'email' => 'required|email',
                            'contact' => 'required|size:10',
                            'queries' => 'required|min:3|max:800'
                        ));
        $enquiry = new Enquiry;

        $enquiry->product_id = $request->input('prodid');
        $enquiry->username = $request->input('name');
        $enquiry->address = $request->input('address');
        $enquiry->email = $request->input('email');
        $enquiry->contact = $request->input('contact');
        $enquiry->query = $request->input('queries');

        $enquiry->save();
        // Session::flash('success','Enquiry message was successfully sent !!');
        
        return redirect()->route('item.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $products = Product::where('slug', '=', $slug )->first();
        return view('Frontend.theme.enquire')->withProducts($products);
    }

    public function showenquiries($id)
    {
        $categories = Category::all();
        $enquiry = Enquiry::find($id); //making status of enquiry seen
        $enquiry->status = 1;
        // $enquiry = Enquiry::find($id);;
        $enquiry->save();
        return view('Enquiries.show')->withenquiry($enquiry);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $enquiry = Enquiry::find($id);
        $enquiry->delete();

        Session::flash('success','Enquiry message was deleted successfully');

        return redirect()->route('enquire.index');
    }
}
