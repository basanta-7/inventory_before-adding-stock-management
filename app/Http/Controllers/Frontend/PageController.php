<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use Session;
use App\Category;

class PageController extends Controller
{
    public function getAbout(){
        $contents = DB::Table('about')->first();
        $categories = Category::orderBy('name')->get();

        // dd($contents);
        return view('Frontend.theme.pages.aboutus')->with('contents',$contents)->with('categories',$categories);
    }

    public function getContact(){
        $categories = Category::orderBy('name')->get();
        return view('Frontend.theme.pages.contact')->with('categories',$categories);
    }

    public function postContact(Request $request){
    	$this->validate($request,[
    					'name' => 'required|string',
    					'email' => 'required|email',
    					'subject' => 'min:5',
    					'message' => 'min:5'
    				]);
    	$data = array(
    		'name' => $request->name,
    		'email' => $request->email,
    		'subject' => $request->subject,
    		'messagebody' => $request->message
    	);

    	Mail::send('Frontend.emails.contact',$data, function($message) use ($data){
    		$message->from($data['email']);
    		$message->to('basantalfc@gmail.com');
    		$message->subject($data['subject']);
    	});

    	Session::flash('success','Your message was successfully sent');

    	return redirect()->route('item.index');
    }
}
