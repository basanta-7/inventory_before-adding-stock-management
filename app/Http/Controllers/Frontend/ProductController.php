<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use Mail;
use Session;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products=Product::orderBy('created_at','desc')->paginate(9);
            $categoryname = "Featured";

            // getting id to get subcategories
            $categories = Category::all();

            // $categoryid = $categories->where('slug', request()->category)->first()->id;
            $categoryid = null;
            $subcategories = null;
            $subcategoriescount = null;
            $select = "";
            $categoryslug =  request()->category;

        $categories = Category::orderBy('name')->get();
        return view('Frontend.theme.index')->with('categories',$categories)->with('products',$products)->with('categoryname',$categoryname)->withsubcategories($subcategories)->with('select',$select)->with('categoryslug',$categoryslug)->with('subcategoriescount',$subcategoriescount);

        //comment starts here 
        // if(request()->category && request()->sortby)
        // {
        //     $products = Product::whereHas('category', function($query){
        //         $query->where('slug',request()->category);
        //     })->orderBy('rate',request('sortby'))->paginate(12)->appends('sortby',request('sortby'));
        //     // $products=Product::orderBy('rate',request('sortby'))->paginate(12)->appends('sortby',request('sortby'));
        //      $select = $request->get('sortby');

        //     if($select == 'default'){
        //         $products = Product::whereHas('category', function($query){
        //         $query->where('slug',request()->category);
        //         })->inRandomOrder()->paginate(12)->appends('sortby',request('sortby'));
        //     }
        //     $categories = Category::all();
        //     $categoryname = $categories->where('slug', request()->category)->first()->name;
            
        //     // getting id to get subcategories
        //     $categoryid = $categories->where('slug', request()->category)->first()->id;
        //     $subcategories = $categories->where('pid', '=', $categoryid );
        //     $subcategoriescount = count($subcategories);

        //     $categoryslug =  request()->category;
        // }
        // elseif(!request()->category && request()->sortby)
        // {
        //     // $products = Product::whereHas('category', function($query){
        //     //     $query->where('slug',request()->category);
        //     // })->orderBy('rate',request('sortby'))->paginate(12)->appends('sortby',request('sortby'));
        //     $products=Product::orderBy('rate',request('sortby'))->paginate(12)->appends('sortby',request('sortby'));
        //      $select = $request->get('sortby');

        //     if($select == 'default'){
        //         $products = Product::inRandomOrder()->paginate(12);
        //     }
        //     $categoryname = "Featured";
            
        //     // getting id to get subcategories
        //     $categories = Category::all();
        //     $categoryid = null;
        //     $subcategories = null;
        //     $subcategoriescount = null;

        //     // $categoryid = $categories->where('slug', request()->category)->first()->id;
        //     // $subcategories = $categories->where('pid', '=', $categoryid );
        //     // $subcategoriescount = count($subcategories);

        //     $categoryslug =  request()->category;
        // }

        // elseif(request()->category)
        // {
        //     $products = Product::whereHas('category', function($query){
        //         $query->where('slug',request()->category);
        //     })->paginate(3);
        //     $categories = Category::all();
        //     $categoryname = $categories->where('slug', request()->category)->first()->name;
        //     $categoryslug =  request()->category;
            
        //     // getting id to get subcategories
        //     $categoryid = $categories->where('slug', request()->category)->first()->id;
        //     $subcategories = $categories->where('pid', '=', $categoryid );
        //     $subcategoriescount = count($subcategories);
        //     $select = "";
        // }
        
        // else{
        //     $products=Product::inRandomOrder()->paginate(12);
        //     $categoryname = "Featured";

        //     // getting id to get subcategories
        //     $categories = Category::all();

        //     // $categoryid = $categories->where('slug', request()->category)->first()->id;
        //     $categoryid = null;
        //     $subcategories = null;
        //     $subcategoriescount = null;
        //     $select = "";
        //     $categoryslug =  request()->category;
        // }

        // $categories = Category::orderBy('name')->get();
        // return view('Frontend.index')->with('categories',$categories)->with('products',$products)->with('categoryname',$categoryname)->withsubcategories($subcategories)->with('select',$select)->with('categoryslug',$categoryslug)->with('subcategoriescount',$subcategoriescount);
    }

    public function getSearch(Request $request)
    {
        if(request()->search)
        {
            $query = request()->search;
            if(request()->sortby == 'default')
            {
                $products = Product::inRandomOrder()->where('name', 'LIKE', '%'.request()->search.'%')->paginate(9);
            }
            elseif(request()->sortby == 'atoz')
            {
                $products = Product::where('name', 'LIKE', '%'.request()->search. '%')->orderBy('name','asc')->paginate(9);
                $select = $request->get('sortby');
            }
            elseif(request()->sortby == 'ztoa')
            {
                $products = Product::where('name', 'LIKE', '%'.request()->search. '%')->orderBy('name','desc')->paginate(9);
                $select = $request->get('sortby');
            }
            else
            {
                $products = Product::where('name', 'LIKE', '%'.request()->search. '%')->orderBy('rate',request('sortby'))->paginate(9);
            }
            $productsforcount = Product::where('name', 'LIKE', '%'.request('search').'%')->get();
            $productcount = count($productsforcount)." results found";
            
            $categories = Category::orderBy('name')->get();
            
            $select = $request->get('sortby');
            return view('Frontend.theme.searchresults')->with('products',$products)->with('categories',$categories)->with('productcount',$productcount)->with('searchquery',$query)->with('select',$select);  
        }    
        else
        {
            $query = $request->get('searchtext');
            $products = Product::where('name', 'LIKE', '%'.$query.'%')->paginate(9);
            $productsforcount = Product::where('name', 'LIKE', '%'.$query.'%')->get();

            $productcount = count($productsforcount)." item found";

            $categories = Category::orderBy('name')->get();
            
            $select = $request->get('sortby');
            return view('Frontend.theme.searchresults')->with('products',$products)->with('categories',$categories)->with('productcount',$productcount)->with('searchquery',$query)->with('select',$select);
        }    
    }

    //getting enquiry message
    public function getEnquire($slug)
    {
        $products = Product::where('slug', '=', $slug )->get();
        return view('Frontend.enquire')->withProducts($products);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product = Product::where('slug', '=', $slug)->first();

        $similarproduct1 = Product::inRandomOrder()->first();
        $similarproduct2 = Product::inRandomOrder()->first();
        $similarproduct3 = Product::inRandomOrder()->first();
        $similarproduct4 = Product::inRandomOrder()->first();

        // $similarproducts = Product::inRandomOrder()->limit(8)->get();

        $categories = Category::orderBy('name')->get();
        return view('Frontend.theme.about')->with('product',$product)->with('categories',$categories)->with('similarproduct1',$similarproduct1)->with('similarproduct2',$similarproduct2)->with('similarproduct3',$similarproduct3)->with('similarproduct4',$similarproduct4);
    }

// get products inside  category
    public function showproducts($slug)
    {
        $categoryname = Category::where('slug', '=', $slug)->first();
        $products = $categoryname->product()->paginate(12);
        
        $categories = Category::orderBy('name')->get();
        
        $categorylist = Category::where('slug', '=', $slug)->get();
        $subcategories = Category::all();


        return view('Frontend.theme.categoryitem')->with('categorylist',$categorylist)->with('categories',$categories)->withsubcategories($subcategories)->with('categoryname',$categoryname)->with('products',$products);
    } 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getstore(Request $request)
    {
        $productcount = Product::get();
        $count=$productcount->count();
        //comment starts here 
        if(request()->category && request()->sortby)
        {
            $products = Product::whereHas('category', function($query){
                $query->where('slug',request()->category);
            })->orderBy('rate',request('sortby'))->paginate(12)->appends('sortby',request('sortby'));
            // $products=Product::orderBy('rate',request('sortby'))->paginate(12)->appends('sortby',request('sortby'));
             $select = $request->get('sortby');

            if($select == 'default'){
                $products = Product::whereHas('category', function($query){
                $query->where('slug',request()->category);
                })->inRandomOrder()->paginate(12)->appends('sortby',request('sortby'));
            }
            $categories = Category::all();
            $categoryname = $categories->where('slug', request()->category)->first()->name;
            
            // getting id to get subcategories
            $categoryid = $categories->where('slug', request()->category)->first()->id;
            $subcategories = $categories->where('pid', '=', $categoryid );
            $subcategoriescount = count($subcategories);

            $categoryslug =  request()->category;
        }
        elseif(!request()->category && request()->sortby)
        {
            if(request()->sortby == 'atoz')
            {
                $products=Product::orderBy('name','asc')->paginate(12)->appends('sortby',request('sortby'));
                $select = $request->get('sortby');
            }
            elseif(request()->sortby == 'ztoa')
            {
                $products=Product::orderBy('name','desc')->paginate(12)->appends('sortby',request('sortby'));
                $select = $request->get('sortby');
            }    
            elseif(request()->sortby == 'default')
            {
                $products = Product::inRandomOrder()->paginate(12);
                $select = $request->get('sortby');
            }
            else
            {
                $products=Product::orderBy('rate',request('sortby'))->paginate(12)->appends('sortby',request('sortby'));
                $select = $request->get('sortby');
            }
            

                $categoryname = "All Products";
                
                // getting id to get subcategories
                $categories = Category::all();
                $categoryid = null;
                $subcategories = null;
                $subcategoriescount = null;

                // $categoryid = $categories->where('slug', request()->category)->first()->id;
                // $subcategories = $categories->where('pid', '=', $categoryid );
                // $subcategoriescount = count($subcategories);

                $categoryslug =  request()->category;
        }

        elseif(request()->category)
        {
            $products = Product::whereHas('category', function($query){
                $query->where('slug',request()->category);
            })->paginate(3);
            $categories = Category::all();
            $categoryname = $categories->where('slug', request()->category)->first()->name;
            $categoryslug =  request()->category;
            
            // getting id to get subcategories
            $categoryid = $categories->where('slug', request()->category)->first()->id;
            $subcategories = $categories->where('pid', '=', $categoryid );
            $subcategoriescount = count($subcategories);
            $select = "";
        }
        
        else{
            $products=Product::orderBy('created_at','desc')->paginate(9);
            $categoryname = "All Products";

            
            // getting id to get subcategories
            $categories = Category::all();

            // $categoryid = $categories->where('slug', request()->category)->first()->id;
            $categoryid = null;
            $subcategories = null;
            $subcategoriescount = null;
            $select = "";
            $categoryslug =  request()->category;
        }
        
        $categories = Category::orderBy('name')->get();
        return view('Frontend.theme.store')->with('categories',$categories)->with('products',$products)->with('categoryname',$categoryname)->withsubcategories($subcategories)->with('select',$select)->with('categoryslug',$categoryslug)->with('subcategoriescount',$subcategoriescount)->with('count',$count)->with('select',$select);
        

        // $categories = Category::orderBy('name')->get();
        // return view('Frontend.theme.store')->with('categories',$categories)->with('products',$products)->with('categoryname',$categoryname)->withsubcategories($subcategories)->with('select',$select)->with('categoryslug',$categoryslug)->with('subcategoriescount',$subcategoriescount)->with('count',$count)->with('categoryname',$categoryname);   
    }

    


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    // posting enquiry messsage cming from form
    // public function postEnquire(Request $request)
    // {
    //     $this->validate($request,array(
    //                         'name' => 'required|regex:/^[a-zA-Z ]+$/|max:50',
    //                         'email' => 'required|email',
    //                         'address' => 'alpha_num',
    //                         'contact' =>'required|digits:10',
    //                         'queries' => 'required|string|min:5'
    //                     ));
    //     $data = array(
    //                 'name' => $request->name,
    //                 'email' => $request->email,
    //                 'address' => $request->address,
    //                 'contact' => $request->contact,
    //                 'queries' => $request->queries,
    //                 'productname' => $request->prodname    
    //             );

    //     Mail::send('Frontend.emails.enquiry',$data,function($message) use ($data){
    //         $message->from($data['email']);
    //         $message->to('basantalfc@gmail.com');
    //         $message->subject($data['queries']);
    //     });

    //     Session::flash('success', 'Your enquiry message was succesfully sent');

    //     return redirect()->route('item.index');
    // }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // // display product belonging to certain category from sidebar
    // public function getCategoryProduct($id){
    //     $products = Product::where('pid', '=', $id)->orderBy('created_at','desc')->paginate(6);
        
    //     $categories = Category::orderBy('name')->get();
    //     $subcategories = Category::where('pid', '=', $id)->get();

    //     return view('Frontend.searchResult')->with('products',$products)->with('categories',$categories)->with('subcategories',$subcategories);
    // }

    // //display product belonging to certain subcategory from tab
    // public function getSubCategoryProduct($id){
    //     $products = Product::where('pid1', '=', $id)->orderBy('created_at','desc')->paginate(6);
        
    //     $categories = Category::orderBy('name')->get();
    //     $subcategories = Category::where('pid', '=', $id)->get();

    //     return view('Frontend.searchSubCategoryResult')->with('products',$products)->with('categories',$categories)->with('subcategories',$subcategories);
    // }
    // public function fetch(Request $request)
    // {
    //     $data = $request->sortby;
    //     $products = null;
    //     // $output = array();
    //     if($data == 'orderhightolow')
    //     {
    //         $products = Product::orderBy('rate','desc')->paginate(9);
    //     }
    //     elseif($data == 'orderlowtohigh')
    //     {
    //         $products = Product::orderBy('rate')->paginate(12);
    //     }
    //     else
    //     {
    //         $products = Product::orderBy('created_at','desc')->paginate(12);
    //     }

    //     // foreach ($products as  $value) {
    //     //     $output[] = '<div class="col-lg-3 col-md-5 card" style="text-align: center;">
    //     //                     <img src="'. asset('images/' . $value['image'] ) .'" class="card-img-top" alt="">
    //     //                      <div class="card-body">
    //     //                         <a href="'. route('item.show', $value->slug) .'" class="card-text" style="font-size: 17px;">'.substr($value->name,0,20) .'</a>
    //     //                         <h6 style="color: orange; font-weight: 600; font-size: 20px;">Rs : '. $value->rate.' </h6>
                               
    //     //                         <a href="'.route('item.show', $value->slug).'">
    //     //                         <div class="btn btn-success btn-block">See More</div></a>
    //     //                      </div>
    //     //                 </div>';
    //     // }

    //     $categories = Category::orderBy('name')->get();
    //     return view('Frontend.index')->with('categories',$categories)->with('products',$products);
    // }
}
